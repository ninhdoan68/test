import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseService} from './base.service';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class TintucService {
  token = this.baseService.tokenApi;
  apiUrl = this.baseService.apiUrl;

  constructor(private httpClient: HttpClient, public baseService: BaseService) {
  }

  getData(table, params) {
    const urlApi = this.apiUrl;
    return this.httpClient.get<any>(encodeURI(`${urlApi}${table}?access_token=${this.token}&filter=${JSON.stringify(params)}`));
  }

  updateViewBlog(prams): Observable<any> {
    return this.httpClient.patch<any>(`${this.baseService.apiUrl}/res_blogs/${prams.id}?access_token=${this.baseService.tokenApi}`, {
      view: prams.view
    });
  }

  updateData(data): Observable<any> {
    return this.httpClient.patch<any>(`${this.baseService.apiUrl}/res_blogs?access_token=${this.baseService.tokenApi}`, data);
  }

  getBlogcategories(slug_cate): Observable<any> {
    const param = {
      where: {
        slug: slug_cate
      },
      include: [
        {
          relation: 'resBlogs',
          scope: {
            where: {
              and: [

                {status: 'active'}
              ]
            },
            order: 'create_date DESC',
            include: [
              {
                relation: 'irAttachments',
                scope: {
                  where: {
                    res_model: 'res.blog',
                  },
                }

              },
              {
                relation: 'resTags',
              },
              {
                relation: 'resBlogCategories',
              },

            ]
          }

        },
      ],
    };
    return this.getData('res_blog_categories', param);
  }

  getBlogcategoriesAll(): Observable<any> {
    const param = {
      where: {
         status : {eq : 'active'}
      },
      include: [
        {
          relation: 'resBlogs',
          scope: {
            where: {
              and: [

                {status: 'active'}
              ]
            },
            order: 'create_date DESC',
            include: [
              {
                relation: 'irAttachments',
                scope: {
                  where: {
                    res_model: 'res.blog',
                  },
                }

              },
              {
                relation: 'resTags',
              },
              {
                relation: 'resBlogCategories',
              }
            ]
          }

        },
      ],
    };
    return this.getData('res_blog_categories', param);
  }

  getBlogbySlug(slug): Observable<any> {
    const param = {
      where: {
        slug,
      },
      include: [
        {
          relation: 'resTags',
        },
        {
          relation: 'irAttachments',
          scope: {
            where: {
              res_model: 'res.blog',
            },
          }

        },
        {
          relation: 'resBlogCategories',
        },
        {
          relation: 'resComments',
          scope: {
            where: {
              status: 'active',
            },
          }
        }
      ]
    };
    return this.getData('res_blogs', param);
  }

  getBlogmoinhat(): Observable<any> {
    const param = {
      where: {},
      include: [
        {
          relation: 'irAttachments',
          scope: {
            where: {
              res_model: 'res.blog',
            },
          }

        },

      ],
      order: 'create_date DESC',
    };
    return this.getData('res_blogs', param);
  }

  addRate(rate): Observable<any> {
    return this.httpClient.post<any>(`${this.baseService.apiUrl}/res_blog_rattings?access_token=${this.baseService.tokenApi}`, {
      id_post: rate.id_post,
      ip: rate.ip,
      rate: rate.rate
    });
  }

  checkRateExit(rate): Observable<any> {
    let params = {
      'where': {and: [{ip: {eq: rate.ip}}, {rate: {eq: rate.rate}}, {id_post: {eq: rate.id_post}}]}
    };
    return this.httpClient.get(`${this.baseService.apiUrl}/res_blog_rattings?access_token=${this.baseService.tokenApi}`, {
      params: {
        filter: JSON.stringify(params)
      }
    });
  }

  checkRatedExit(rate): Observable<any> {
    let params = {
      'where': {and: [{ip: {eq: rate.ip}}, {id_post: {eq: rate.id_post}}]}
    };
    return this.httpClient.get(`${this.baseService.apiUrl}/res_blog_rattings?access_token=${this.baseService.tokenApi}`, {
      params: {
        filter: JSON.stringify(params)
      }
    });
  }

  updateRate(rate_update): Observable<any> {
    return this.httpClient.patch<any>(`${this.baseService.apiUrl}/res_blog_rattings/${rate_update.id}?access_token=${this.baseService.tokenApi}`, {
      rate: rate_update.rate
    });
  }

  countRateByPost(id_post): Observable<any> {
    let params = {
      'where': {
        and: [
          {id_post: {eq: id_post}},
          {rate: {neq: null}}
        ]
      }
    };
    return this.httpClient.get(`${this.baseService.apiUrl}/res_blog_rattings?access_token=${this.baseService.tokenApi}`, {
      params: {
        filter: JSON.stringify(params)
      }
    });
  }

  getListCommentByBlog(blog_id) {
    const param = {};
    const urlApi: any = this.getUrlListCommentByBlog(param, blog_id);
    return this.httpClient.get(urlApi);
  }

  getUrlListCommentByBlog(param, hotel_id) {
    return `${this.apiUrl}res_comments?filter[where][obj_id]=${hotel_id}&access_token=${this.token}`;
  }

  isEmail(email): any {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  send_request_rate(params): Observable<any> {
    const urlApi: any = `${this.apiUrl}res_comments?access_token=${this.token}`;
    return this.httpClient.post<any>(urlApi, params);
  }


}

