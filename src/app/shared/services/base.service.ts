import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';
import {environment} from '../../../environments/environment';
import { PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class BaseService {

  baseUrl;
  apiUrl;
  adminUrl;
  tokenApi;
  constructor(@Inject(PLATFORM_ID) private _platformId: Object,
              private httpClient: HttpClient, ) {
    this.getBaseUrl();
  }

  getBaseUrl() {
    if (isPlatformBrowser(this._platformId)) {
      this.baseUrl = environment['client'].baseUrl;
      this.apiUrl = environment['client'].apiUrl;
      this.adminUrl = environment['client'].adminUrl;
      this.tokenApi = environment['client'].tokenApi;
    }
    if (isPlatformServer(this._platformId)) {
      this.apiUrl = environment['server'].apiUrl;
      this.adminUrl = environment['server'].adminUrl;
      this.baseUrl = environment['server'].baseUrl;
      this.tokenApi = environment['server'].tokenApi;
    }
  }

  getData(table, params) {
    const urlApi = this.apiUrl;
    return this.httpClient.get<any>(encodeURI(`${urlApi}${table}?access_token=${this.tokenApi}&filter=${JSON.stringify(params)}`));
  }

  getPartnerss(): Observable<any> {
    let params = {
      where: {
        id: "1"
      }
    };

    return this.getData('res_partners', params);
  }

  updatePartner(partner): Observable<any> {
    return this.httpClient.patch<any>(`${this.apiUrl}/res_partners/${partner.id}?access_token=${this.tokenApi}`, {
      name: partner.name,
      commercial_partner_id: partner.id,
      id: partner.id
    });
  }
}
