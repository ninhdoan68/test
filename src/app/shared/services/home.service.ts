import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseService} from './base.service';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class HomeService {
  token = this.baseService.tokenApi;
  apiUrl = this.baseService.apiUrl;

  constructor(private httpClient: HttpClient, public baseService: BaseService, ) {
  }

  getData(table, params) {
    const urlApi = this.apiUrl;
    return this.httpClient.get<any>(encodeURI(`${urlApi}${table}?access_token=${this.token}&filter=${JSON.stringify(params)}`));
  }

  getPartnerss(): Observable<any> {
    let params = {
      where: {
        id: "1"
      }
    };

    return this.getData('res_partners', params);
  }

  getHomeLoGo(): Observable<any> {
    const param = {
      where: {},
      include: [
        {
          relation: 'irAttachments',
          scope: {
            where: {
              res_model: 'res.banner'
            },
            order: 'id DESC'
          }
        },
      ],
    };
    return this.getData('res_banners', param);
  }

  getHomeBanner(): Observable<any> {
    const param = {
      where: {
        or: [{slug : {like: '%home-banner-slider%'}}, {slug: {like: '%home-gallery%'}}]
      },
      include: [
        {
          relation: 'resMedia',
          scope: {
            include: [
              {
                relation: 'irAttachments',
                scope: {
                  where: {
                    res_model: 'res.media'
                  }
                }
              }
            ]
          }
        }
      ]
    };
    return this.getData('res_galleries', param);
  }

  getBlogcategories(): Observable<any> {
    const param = {
      include: [
        {
          relation: 'resBlogs',
          scope: {
            where: {
              and: [

                {status: 'active'}
              ]
            },
            order: 'write_date DESC',
            include: [
              {
                relation: 'resTags'
              },
              {
                relation: 'irAttachments',
                scope: {
                  where: {
                    res_model: "res.blog",
                  },
                }

              }
            ]
          }

        },
      ],
    };
    return this.getData('res_blog_categories', param);
  }

  getHotel(): Observable<any> {
    const param = {
      where: {

      },
      include: [
        {relation: 'resPromotions',
        order: 'check_in DESC',
      },
        {
          relation: 'irAttachments',
          scope: {
            where: {
              res_model: "hotel.room.type",
            },
          }

        }
      ]
    };
    return this.getData('hotel_room_types', param);
  }

  getCombo(): Observable<any> {
    const param = {
      where: {},
      include: [
        {
          relation: 'irAttachments',
          scope: {
            where: {
              res_model: { eq: 'res.combo' }
            },
            order: 'id DESC'
          }
        },
        {
          relation: 'resPromotions'
        }
      ]
    };
    return this.getData('res_combos', param);
  }

  getDichvu(): Observable<any> {
    const param = {
      where: {},

    };
    return this.getData('product_products', param);
  }

  getHotelRoomType(): Observable<any> {
    const param = {};
    return this.getData('hotel_room_types', param);
  }
  getTypeofRoom(): Observable<any> {
    const param = {
      include: [
        {
          relation: 'irAttachments',
          scope: {
            where: {
              res_model: { eq: 'res.type.room' }
            },
            order: 'id DESC'
          }
        },
        {
          relation: 'hotelRoomTypes'
        }
      ]
    };
    return this.getData('res_type_rooms', param);
  }
  getAboutUs():Observable<any>{
    const param={
      // where:{
      //   name:'VỀ THIÊN SƠN SUỐI NGÀ'
      // }
    }
    return this.getData('res_about_us',param);
  }
  postContact(partners): Observable<any> {
    let data = {
      "name": partners.name,
      "display_name": partners.name,
      "date": new Date().toISOString(),
      "active": true,
      "customer": true,
      "email": partners.email,
      "phone": "",
      "is_company": true,
      "partner_share": true,
      "invoice_warn": "no-message",
      "invoice_warn_msg": "string",
      "sale_warn": "no-message",
      "picking_warn": "no-message",
      "picking_warn_msg": "string",
      "purchase_warn": "no-message"

    };

    return this.httpClient.post(`${this.apiUrl}res_partners?access_token=${this.token}`,
      data
    );
  }

  addCrmLead(param) {
    let data = {
      "email_from": param.email,
      "name": "Yêu cầu tư vấn",
      "type": "lead"
    };
    console.log(data);

    return this.httpClient.post(`${this.apiUrl}crm_leads?access_token=${this.token}`,
      data
    );
  }
}

