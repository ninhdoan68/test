import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseService} from './base.service';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class DatPhongService {
  token = this.baseService.tokenApi;
  apiUrl = this.baseService.apiUrl;
  data = {
    start_date: null,
    end_date: null,
    number: 1
  };

  constructor(private httpClient: HttpClient, public baseService: BaseService, ) {
  }

  getData(table, params) {
    const urlApi = this.apiUrl;
    return this.httpClient.get<any>(encodeURI(`${urlApi}${table}?access_token=${this.token}&filter=${JSON.stringify(params)}`));
  }

  getHeroBannerBooking(): Observable<any> {
    const param = {
      where: {
        slug: {eq: 'banner-datphong-khach-san'}
      },
      include: [
        {
          relation: 'irAttachments',
          scope: {
            where: {
              res_model: {eq: 'res.banner'}
            },
            order: 'id DESC'
          }

        },
      ],
    };
    return this.getData('res_banners', param);
  }

  getImageUrl(model, id: any, res_field = 'image') {
    return this.httpClient.get<any>(`${this.apiUrl}ir_attachments`, {
      params: {
        'access_token': this.token,
        'filter[where][res_id]': id,
        'filter[where][res_model][like]': model,
        'filter[where][res_field][like]': res_field
      },
      observe: 'response'
    });
  }

  getBooking(): Observable<any> {
    const param = {
      where: {
        and: [{slug : {nlike: '%phong-hop%'}}, {slug: {nlike: '%nha-san%'}}]
      },
      include: [
        {
          relation: 'irAttachments',
          scope: {
            where: {
              res_model: {eq: 'hotel.room.type'}
            },
            order: 'id DESC'
          }
        },
        {
          relation: 'stockWarehouse',
        },
        {
          relation: 'hotelRooms',
        },
        {
          relation: 'hotelRoomAmenities',
          scope: {
            include: {
              relation: 'irAttachments',
              scope: {
                where: {
                  res_model: {eq: 'hotel.room.amenities'}
                },
                order: 'id DESC'
              }
            }
          }
        },
      ],
    };
    return this.getData('hotel_room_types', param);
  }

  getDetailBooking(slug) {
    const date = new Date();
    const param = {
      where: {
        slug: {eq: slug}
      },
      include: [
        {
          relation: 'irAttachments',
          scope: {
            where: {
              res_model: {eq: 'hotel.room.type'}
            },
            order: 'id DESC'
          }
        },
        {
          relation: 'hotelRooms',
        },
        {
          relation: 'stockWarehouse',
        },
        {
          relation: 'resGalleries',
          scope: {
            include: {
              relation: 'resMedia',
              scope: {
                include: {
                  relation: 'irAttachments',
                  scope: {
                    where: {
                      res_model: {eq: 'res.media'}
                    },
                    order: 'id DESC'
                  }
                }
              }
            },
          }
        },
        {
          relation: 'resPromotions',
          scope: {
            where: {
              book_date_from: {lte: date.toISOString()},
              book_date_to: {gte: date.toISOString()},
            },
            order: 'id DESC'
          }
        },
        {
          relation: 'hotelRoomAmenities',
          scope: {
            include: {
              relation: 'irAttachments',
              scope: {
                where: {
                  res_model: {eq: 'hotel.room.amenities'}
                },
                order: 'id DESC'
              }
            }
          }
        },
      ],
      limit: 1

    };
    return this.getData('hotel_room_types', param);
  }

  getRoomName(slug) {
    const param = {
      where: {
        slug: {eq: slug}
      },
      fields: {name: true, slug: true}
    };
    return this.getData('hotel_room_types', param);
  }

  getPartnerByMobile(mobile): Observable<any> {
    const urlApi: any = `${this.apiUrl}res_partners?filter[where][mobile]=${mobile}&access_token=${this.token}`;
    return this.httpClient.get(urlApi);
  }

  getXemGia() {

  }

  getPhongGiadinhFolio(): Observable<any> {
    const param = {
      order: 'id DESC',
      limit: 1,
    };
    return this.getData('hotel_folios', param);
  }

  savePhongGiadinhFolio(data): Observable<any> {
    return this.httpClient.post<any>(`${this.apiUrl}/hotel_folios?access_token=${this.token}`, data);
  }

  getHotelReservations(): Observable<any> {
    const param = {
      order: 'id DESC',
      limit: 1,
    };
    return this.getData('hotel_reservations', param);
  }

  saveHotelReservations(data): Observable<any> {
    return this.httpClient.post<any>(`${this.apiUrl}/hotel_reservations?access_token=${this.token}`, data);
  }

  send_request_partner(params): Observable<any> {
    const urlApi: any = `${this.apiUrl}res_partners?access_token=${this.token}`;
    return this.httpClient.post<any>(urlApi, params);
  }

  isEmail(email): any {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  getProductHotelAmenity(id): Observable<any> {
    const param = {
      where: {
        and: [
          {id: {eq: id}}
          // {res_model: { eq: 'res.event.line' }}
        ]
      },
    };
    return this.getData('product_products', param);
  }

  getBank(): Observable<any> {
    const param = {
      include: [
        {
          relation: 'resBank',
          scope: {
            include: [
              {
                relation: 'irAttachments',
                scope: {
                  where: {
                    res_model: {eq: 'res.bank'}
                  },
                  order: 'id DESC'
                }
              },
            ]
          }
        },
        {
          relation: 'resPartner',
        },
      ]
    };
    return this.getData('res_partner_banks', param);
  }
}
