import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseService} from './base.service';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ThankyouService {
  token = this.baseService.tokenApi;
  apiUrl = this.baseService.apiUrl;

  constructor(private httpClient: HttpClient, public baseService: BaseService, ) { }

  async getDataasync(table, params) {
    const urlApi = this.apiUrl;
    let response = await this.httpClient.get<any>(encodeURI(`${urlApi}${table}?access_token=${this.token}&filter=${JSON.stringify(params)}`)).toPromise();
    return response;
  }

  getData(table, params) {
    const urlApi = this.apiUrl;
    return this.httpClient.get<any>(encodeURI(`${urlApi}${table}?access_token=${this.token}&filter=${JSON.stringify(params)}`));
  }

  getHeroBannerCombo(): Observable<any> {
    const param = {
      where: {
        slug: {eq: 'banner-thank-you'}
      },
      include: [
        {
          relation: 'irAttachments',
          scope: {
            where: {
              res_model: { eq: 'res.banner' }
            },
            order: 'id DESC'
          }
        },
      ],
    };
    return this.getData('res_banners', param);
  }
}
