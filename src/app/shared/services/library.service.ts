import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { BaseService } from '../../shared/services/base.service';
@Injectable({
  providedIn: 'root'
})
export class LibraryService {

  constructor(private http: HttpClient, private baseUrl: BaseService) { }
  apiUrl = this.baseUrl.apiUrl;
  token = this.baseUrl.tokenApi;
  getData(table, params) {
    const apiURL = this.apiUrl;
    return this.http.get<any>(encodeURI(`${apiURL}${table}?access_token=${this.token}&filter=${JSON.stringify(params)}`));
  }
  getImage(): Observable<any> {
    const param = {
      where: {
        slug: "thu-vien-anh",
      },
      include: [
        {
          relation: 'resGalleries',
          scope: {
            include: {
              relation: 'resMedia',
              scope: {
                include: {
                  relation: 'irAttachments',
                  scope: {
                    where: {
                      res_model: { eq: 'res.media' }
                    },
                    order: 'id DESC'
                  }
                }
              }
            },
          }
        }
      ],
    };
    return this.getData('res_gallery_categories', param);
  }
  getVideo(): Observable<any> {
    const param = {
      where: {
        slug: 'thu-vien-video',
      },
      include: [
        {
          relation: 'resGalleries',
          scope: {
            include: {
              relation: 'resMedia',
              scope: {
                include: {
                  relation: 'irAttachments',
                  scope: {
                    where: {
                      res_model: { eq: 'res.media' }
                    },
                    order: 'id DESC'
                  }
                }
              }
            },
          }
        }
      ],
    };
    return this.getData('res_gallery_categories', param);
  }
  getImageUrl(model, id: any, res_field = 'image') {
    const apiURL = this.apiUrl;
    return this.http.get<any>(`${apiURL}ir_attachments`, {
      params: {
        'access_token': this.token,
        'filter[where][res_id]': id,
        'filter[where][res_model][like]': model,
        'filter[where][res_field][like]': res_field
      },
      observe: 'response'
    });
  }
}
