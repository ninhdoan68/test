import {
  Injectable
} from '@angular/core';
import {
  HttpClient
} from '@angular/common/http';
import {
  Observable
} from 'rxjs';
import {
  BaseService
} from '../../shared/services/base.service';
@Injectable({
  providedIn: 'root'
})
export class ContactService {

  constructor(
    private http: HttpClient,
    private baseUrl: BaseService
  ) {}
  apiUrl = this.baseUrl.apiUrl;
  token = this.baseUrl.tokenApi;
  getData(table, params) {
    const apiURL = this.apiUrl;
    return this.http.get < any > (encodeURI(`${apiURL}${table}?access_token=${this.token}&filter=${JSON.stringify(params)}`));
  }
  getPartnerByEmail(email): Observable<any> {
    const urlApi: any = `${this.apiUrl}res_partners?filter[where][email]=${email}&access_token=${this.token}`;
    return this.http.get(urlApi);
  }
  getContact(): Observable < any > {
    const param = {
      where: {
        name: "Thiên Sơn Suối Ngà",
      },
      limit: 1,
    };
    return this.getData('res_partners', param);
  }
  getBanner(): Observable < any > {
    const param = {
      where: {
        slug: 'banner-lien-he',
      },
      limit: 1,
      include: [{
        relation: 'irAttachments',
        scope: {
          where: {
            res_model: "res.banner",
            name: 'image'
          }
        }
      }]
    };
    return this.getData('res_banners', param);
  }
  // post partner
  // post crm
  postContact(partner): Observable < any > {
    let data = {
      "name": partner.name,
      "display_name": partner.name,
      "email": partner.email,
      "date": new Date().toISOString(),
      "comment": partner.massage,
      "invoice_warn": "no-message",
      "invoice_warn_msg": "",
      "sale_warn": "no-message",
      "sale_warn_msg": "",
      "picking_warn": "no-message",
      "picking_warn_msg": "",
    };

    return this.http.post(`${this.apiUrl}res_partners?access_token=${this.token}`, data);
  }

  addCrmLead(partners) {
    console.log('aa', partners);

    let data = {
      "name": partners.name,
      "email_from": partners.email,
      "description": partners.description,
      "type": "opportunity",
      "partner_id": partners.partner_id
    };
    return this.http.post(`${this.apiUrl}crm_leads?access_token=${this.token}`,data);
  }
}
