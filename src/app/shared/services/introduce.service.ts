import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
@Injectable({
  providedIn: 'root'
})
export class IntroduceService {
  token = this.baseService.tokenApi;
  apiUrl = this.baseService.apiUrl;
  constructor( public baseService: BaseService, private httpClient: HttpClient) { }
  getData(table, params) {
    const urlApi = this.apiUrl;
    return this.httpClient.get<any>(encodeURI(`${urlApi}${table}?access_token=${this.token}&filter=${JSON.stringify(params)}`));
  }

  postContact(partner): Observable < any > {
    let data = {
      "name": partner.name,
      "display_name": partner.name,
      "email": partner.email,
      "date": new Date().toISOString(),
      "comment": partner.massage,
      "invoice_warn": "no-message",
      "invoice_warn_msg": "",
      "sale_warn": "no-message",
      "sale_warn_msg": "",
      "picking_warn": "no-message",
      "picking_warn_msg": "",
    };

    return this.httpClient.post(`${this.apiUrl}res_partners?access_token=${this.token}`, data);
  }
  addCrmLead(partners) {
    console.log('partners: ', partners);

    let data = {
      "name": partners.name,
      "email_from": partners.email,
      "description": partners.description,
      "type": "opportunity",
      "partner_id": partners.partner_id
    };
    return this.httpClient.post(`${this.apiUrl}crm_leads?access_token=${this.token}`,data);
  }
  getPartnerByEmail(email): Observable<any> {
    const urlApi: any = `${this.apiUrl}res_partners?filter[where][email]=${email}&access_token=${this.token}`;
    return this.httpClient.get(urlApi);
  }
  getBanner(): Observable<any> {
    const param = {

      include: {
        relation: 'irAttachments',
        scope: {
          where: {
          res_model: 'res.banner'
          }
        }
      }
    };
    return this.getData('res_banners', param);
  }
  getAboutUs(): Observable<any> {
    const param = {
      include: [
        {
          relation: 'irAttachments',
          scope: {
            where: {
              res_model: 'res.about.us'
            }
          }
      },
        {
        relation: 'resGalleries',
        scope: {
          include: {
            relation: 'resMedia',
            scope: {
              include: {
                relation: 'irAttachments',
                scope: {
                  where: {
                    res_model: 'res.media'
                  }
                }
              }
            }

            }
          }
        }]

    };
    return this.getData('res_about_us', param);
  }
}
