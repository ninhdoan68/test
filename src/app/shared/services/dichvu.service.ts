import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseService} from './base.service';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class DichvuService {
  token = this.baseService.tokenApi;
  apiUrl = this.baseService.apiUrl;

  constructor(private httpClient: HttpClient, public baseService: BaseService, ) {   }

  getData(table, params) {
    const urlApi = this.apiUrl;
    return this.httpClient.get<any>(encodeURI(`${urlApi}${table}?access_token=${this.token}&filter=${JSON.stringify(params)}`));
  }

getDichvu(): Observable<any> {
    const param = {
      where: {
        isservice: "true"
      },
      include:[
        {
          relation: 'productTemplate',
          scope: {
            include:[
              {
                relation: 'productUom',
          
              },
            ]
          }
        },
        {
            relation:'irAttachments',
            scope:{
              where:{
                res_model: "product.product",
              },
          },
        }
      ]

    };
    return this.getData('product_products', param);
  }
}