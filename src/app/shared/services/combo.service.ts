import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseService} from './base.service';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ComboService {
  token = this.baseService.tokenApi;
  apiUrl = this.baseService.apiUrl;

  constructor(private httpClient: HttpClient, public baseService: BaseService, ) { }

  async getDataasync(table, params) {
    const urlApi = this.apiUrl;
    let response = await this.httpClient.get<any>(encodeURI(`${urlApi}${table}?access_token=${this.token}&filter=${JSON.stringify(params)}`)).toPromise();
    return response;
  }

  getData(table, params) {
    const urlApi = this.apiUrl;
    return this.httpClient.get<any>(encodeURI(`${urlApi}${table}?access_token=${this.token}&filter=${JSON.stringify(params)}`));
  }

  updatePartner(partner): Observable<any> {
    return this.httpClient.patch<any>(`${this.baseService.apiUrl}/res_partners/${partner.id}?access_token=${this.baseService.tokenApi}`, {
      name: partner.name,
      commercial_partner_id: partner.id,
      id: partner.id
    });
  }

  getHeroBannerCombo(): Observable<any> {
    const param = {
      where: {
        slug: {eq: 'banner-combo'}
      },
      include: [
        {
          relation: 'irAttachments',
          scope: {
            where: {
              res_model: { eq: 'res.banner' }
            },
            order: 'id DESC'
          }
        },
      ],
    };
    return this.getData('res_banners', param);
  }

  getProductCombo(id) {
    const param = {
      where: {
        and: [
          {res_id: { eq: id }},
          {res_model: { eq: 'res.combo' }}
        ]
      },
    };
    return this.getData('product_products', param);
  }

  getCombo(): Observable<any> {
    const date = new Date();
    const param = {
      include: [
        {
          relation: 'irAttachments',
          scope: {
            where: {
              res_model: { eq: 'res.combo' }
            },
            order: 'id DESC'
          }
        },
        {
          relation: 'resPromotions',
          scope: {
            where: {
              book_date_from: { lte: date.toISOString() },
              book_date_to: { gte: date.toISOString() },
            },
            order: 'id DESC'
          }
        }
      ],
    };
    return this.getData('res_combos', param);
  }

  getDetailCombo(slug) {
    const date = new Date();
    const param = {
      where: {
        slug: { eq: slug }
      },
      include: [
        {
          relation: 'irAttachments',
          scope: {
            where: {
              res_model: { eq: 'res.combo' }
            },
            order: 'id DESC'
          }
        },
        {
          relation: 'resComboIncludes',
          scope:{
            where:{
              status: "active"
            }
          }
        },
        {
          relation: 'stockWarehouse',
        },
        {
          relation: 'resGalleries',
          scope: {
            include: {
              relation: 'resMedia',
              scope: {
                include: {
                  relation: 'irAttachments',
                  scope: {
                    where: {
                      res_model: { eq: 'res.media' }
                    },
                    order: 'id DESC'
                  }
                }
              }
            },
          }
        },
        {
          relation: 'resPromotions',
          scope: {
            where: {
              book_date_from: { lte: date.toISOString() },
              book_date_to: { gte: date.toISOString() },
            },
            order: 'id DESC'
          }
        }
      ],
      limit: 1

    };
    return this.getData('res_combos', param);
  }

  getComboName(slug) {
    const param = {
      where: {
        slug: { eq: slug }
      },
      fields : {name : true, slug : true}
    };
    return this.getData('res_combos', param);
  }

  getPartnerByMobile(mobile): Observable<any> {
    const urlApi: any = `${this.apiUrl}res_partners?filter[where][mobile]=${mobile}&access_token=${this.token}`;
    return this.httpClient.get(urlApi);
  }

  getHotelFolio(): Observable<any> {
    const param = {
      order: 'id DESC',
      limit: 1,
    };
    return this.getData('hotel_folios', param);
  }

  getSaleOrder(): Observable<any> {
    const param = {
      order: 'id DESC',
      limit: 1,
    };
    return this.getData('sale_orders', param);
  }

  saveSaleOrder(data): Observable<any> {
    return this.httpClient.post<any>(`${this.apiUrl}/sale_orders?access_token=${this.token}`, data);
  }

  saveSaleOrderLine(data: any): Observable<any> {
    return this.httpClient.post<any>(`${this.apiUrl}/sale_order_lines?access_token=${this.token}`, data);
  }

  saveHotelServiceLine(data: any): Observable<any> {
    return this.httpClient.post<any>(`${this.apiUrl}/hotel_service_lines?access_token=${this.token}`, data);
  }

  saveHotelFolio(data): Observable<any> {
    return this.httpClient.post<any>(`${this.apiUrl}/hotel_folios?access_token=${this.token}`, data);
  }

  send_request_partner(params): Observable<any> {
    const urlApi: any = `${this.apiUrl}res_partners?access_token=${this.token}`;
    return this.httpClient.post<any>(urlApi, params);
  }

  isEmail(email): any {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
}
