import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {BaseService} from '../../shared/services/base.service';

@Injectable({
  providedIn: 'root'
})
export class CuisineService {

  constructor(
    private http: HttpClient,
    private bs: BaseService,
  ) {
  }

  apiUrl = this.bs.apiUrl;
  token = this.bs.tokenApi;

  getData(table, params) {
    const apiURL = this.apiUrl;
    return this.http.get<any>(encodeURI(`${apiURL}${table}?access_token=${this.token}&filter=${JSON.stringify(params)}`));
  }

  getCuisines(): Observable<any> {
    let param = {
      include: [
        {
          relation: 'resGalleries',
          scope: {
            include: [
              {
                relation: 'resMedia',
                scope: {
                  include: [
                    {
                      relation: 'irAttachments',
                      scope: {
                        where: {
                          res_model: "res.media",
                        }
                      }
                    }
                  ]
                }
              }
            ]
          }
        },
        {
          relation: 'irAttachments',
          scope: {
            where: {
              res_model: "res.cuisine"
            }
          }
        }
      ]
    };
    return this.getData('res_cuisines', param);
  }

  getProductCuisine(id): Observable<any> {
    const param = {
      where: {
        and: [
          {res_id: { eq: id }},
          {res_model: { eq: 'res.cuisine' }}
        ]
      },
    };
    return this.getData('product_products', param);
  }

  getCuisine(slug): Observable<any> {
    let param = {
      where: {slug},
      include: [
        {
          relation: 'resGalleries',
          scope: {
            include: [
              {
                relation: 'resMedia',
                scope: {
                  include: [
                    {
                      relation: 'irAttachments',
                      scope: {
                        where: {
                          res_model: "res.media",
                        }
                      }
                    }
                  ]
                }
              }
            ]
          }
        },
        {
          relation: 'irAttachments',
          scope: {
            where: {
              res_model: "res.cuisine"
            }
          }
        }
      ]
    };
    return this.getData('res_cuisines', param);
  }

  getCuisineName(slug): Observable<any> {
    const param = {
      where: {
        slug: { eq: slug }
      },
      fields : {name : true, slug : true}
    };
    return this.getData('res_cuisines', param);
  }

  getBeverage(): Observable<any> {
    let param = {
      where: {
        slug: "do-uong",
      },
      include: [
        {
          relation: 'hotelMenucards',
          scope: {
            where: {
              cuisine_type: "drink"
            },
            include: [
              {
                relation: 'irAttachments',
                scope: {
                  where: {
                    res_model: "hotel.menucard"
                  }
                }
              },
              {
                relation: 'productProduct',
                scope: {
                  include: [
                    {
                      relation: 'productTemplate',
                      scope: {
                        include: [
                          {
                            relation: 'productUom',
                          }
                        ]
                      }
                    }
                  ]
                }
              }
            ]
          }

        }
      ]
    };
    return this.getData('hotel_menucard_types', param);
  }

  getRestaurant(): Observable<any> {
    let param = {
      where: {
        slug: "nha-hang",
      },
      include: [
        {
          relation: 'hotelMenucards',
          scope: {
            where: {
              cuisine_type: "restaurant"
            },
            include: [
              {
                relation: 'irAttachments',
                scope: {
                  where: {
                    res_model: "hotel.menucard"
                  }
                }
              },
              {
                relation: 'productProduct',
                scope: {
                  include: [
                    {
                      relation: 'productTemplate',
                      // scope:{
                      //   include:[
                      //     {
                      //       relation: 'productUom',
                      //     }
                      //   ]
                      // }
                    }
                  ]
                }
              }
            ]
          }

        }
      ]
    };
    return this.getData('hotel_menucard_types', param);
  }

  getFood(): Observable<any> {
    let param = {
      where: {
        slug: "dac-san",
      },
      include: [
        {
          relation: 'hotelMenucards',

          scope: {
            where: {
              cuisine_type: "dacsan"
            },
            include: [
              {
                relation: 'irAttachments',
                scope: {
                  where: {
                    res_model: "hotel.menucard"
                  }
                }
              },
              {
                relation: 'productProduct',
                scope: {
                  include: [
                    {
                      relation: 'productTemplate',
                    }
                  ]
                }
              }
            ]
          }
        }
      ]
    };
    return this.getData('hotel_menucard_types', param);
  }

  getCuisine2(slug): Observable<any> {
    let param = {
      where: {slug},

      include: [
        {
          relation: 'irAttachments',
          scope: {
            where: {
              res_model: "res.media",
            }
          }
        }
      ]


    };
    return this.getData('res_media', param);
  }

  getCuisine3(): Observable<any> {
    let param = {
      include: [
        {
          relation: 'irAttachments',
          scope: {
            where: {
              res_model: "res.cuisine",
            }
          }
        }
      ]
    };
    return this.getData('res_cuisines', param);
  }

  isEmail(email): any {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  getPartnerByMobile(mobile): Observable<any> {
    const urlApi: any = `${this.apiUrl}res_partners?filter[where][mobile]=${mobile}&access_token=${this.token}`;
    return this.http.get(urlApi);
  }

  getHotelFolio(): Observable<any> {
    const param = {
      order: 'id DESC',
      limit: 1,
    };
    return this.getData('hotel_folios', param);
  }

  getSaleOrder(): Observable<any> {
    const param = {
      order: 'id DESC',
      limit: 1,
    };
    return this.getData('sale_orders', param);
  }

  saveSaleOrderLine(data: any): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/sale_order_lines?access_token=${this.token}`, data);
  }

  saveHotelServiceLine(data: any): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/hotel_service_lines?access_token=${this.token}`, data);
  }

  send_request_partner(params): Observable<any> {
    const urlApi: any = `${this.apiUrl}res_partners?access_token=${this.token}`;
    return this.http.post<any>(urlApi, params);
  }

  saveHotelFolio(data): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/hotel_folios?access_token=${this.token}`, data);
  }

  saveSaleOrder(data): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/sale_orders?access_token=${this.token}`, data);
  }
}
