import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseService} from './base.service';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class EventService {
  token = this.baseService.tokenApi;
  apiUrl = this.baseService.apiUrl;

  constructor(private httpClient: HttpClient, public baseService: BaseService, ) { }

  async getDataasync(table, params) {
    const urlApi = this.apiUrl;
    let response = await this.httpClient.get<any>(encodeURI(`${urlApi}${table}?access_token=${this.token}&filter=${JSON.stringify(params)}`)).toPromise();
    return response;
  }

  getIndexOfObjectInArray(item, listItem, key): any {
    for (let i = 0; i < listItem.length; i++) {
      const currentItem = listItem[i];
      if (item[key] === currentItem[key]) {
        return i;
      }
    }
    return null;
  }

  checkExistsObjectInArrayObject(item, listItem, key): any {
    for (let i = 0; i < listItem.length; i++) {
      const currentItem = listItem[i];
      if (item[key] === currentItem[key]) {
        return true;
      }
    }
    return false;
  }

  getData(table, params) {
    const urlApi = this.apiUrl;
    return this.httpClient.get<any>(encodeURI(`${urlApi}${table}?access_token=${this.token}&filter=${JSON.stringify(params)}`));
  }

  getEvent(): Observable<any> {
    const param = {
      include: [
        {
          relation: 'irAttachments',
          scope: {
            where: {
              res_model: { eq: 'res.event' }
            },
            order: 'id DESC'
          }
        },
        // {
        //   relation: 'resPromotions'
        // }
      ],
    };
    return this.getData('res_events', param);
  }

  getDetailEvent(slug): Observable<any> {
    const param = {
      where: {
        slug: { eq: slug }
      },
      include: [
        {
          relation: 'irAttachments',
          scope: {
            where: {
              res_model: { eq: 'res.event' }
            },
            order: 'id DESC'
          }
        },
        {
          relation: 'resGalleries',
          scope: {
            include: {
              relation: 'resMedia',
              scope: {
                include: {
                  relation: 'irAttachments',
                  scope: {
                    where: {
                      res_model: { eq: 'res.media' }
                    },
                    order: 'id DESC'
                  }
                }
              }
            },
          }
        },
        {
          relation: 'resEventLines',
        },
        {
          relation: 'resEventAddons',
        },
        {
          relation: 'resAdditionalServices',
        },


      ],
      limit: 1

    };
    return this.getData('res_events', param);
  }

  getEventName(slug) {
    const param = {
      where: {
        slug: { eq: slug }
      },
      fields : {name : true, slug : true}
    };
    return this.getData('res_events', param);
  }

  getProductEvent(id): Observable<any> {
    const param = {
      where: {
        and: [
          {res_id: { eq: id }},
          {res_model: { eq: 'res.event.line' }}
        ]
      },
    };
    return this.getData('product_products', param);
  }

  getProductservice(id): Observable<any> {
    const param = {
      where: {
        and: [
          {res_id: { eq: id }},
          {res_model: { eq: 'res.additional.service' }}
        ]
      },
    };
    return this.getData('product_products', param);
  }

  getPartnerByMobile(mobile): Observable<any> {
    const urlApi: any = `${this.apiUrl}res_partners?filter[where][mobile]=${mobile}&access_token=${this.token}`;
    return this.httpClient.get(urlApi);
  }

  getHotelFolio(): Observable<any> {
    const param = {
      order: 'id DESC',
      limit: 1,
    };
    return this.getData('hotel_folios', param);
  }

  getSaleOrder(): Observable<any> {
    const param = {
      order: 'id DESC',
      limit: 1,
    };
    return this.getData('sale_orders', param);
  }

  saveSaleOrder(data): Observable<any> {
    return this.httpClient.post<any>(`${this.apiUrl}/sale_orders?access_token=${this.token}`, data);
  }

  saveSaleOrderLine(data: any): Observable<any> {
    return this.httpClient.post<any>(`${this.apiUrl}/sale_order_lines?access_token=${this.token}`, data);
  }

  saveHotelServiceLine(data: any): Observable<any> {
    return this.httpClient.post<any>(`${this.apiUrl}/hotel_service_lines?access_token=${this.token}`, data);
  }

  saveHotelFolio(data): Observable<any> {
    return this.httpClient.post<any>(`${this.apiUrl}/hotel_folios?access_token=${this.token}`, data);
  }

  send_request_partner(params): Observable<any> {
    const urlApi: any = `${this.apiUrl}res_partners?access_token=${this.token}`;
    return this.httpClient.post<any>(urlApi, params);
  }
  isEmail(email): any {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

}
