import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BreadcrumbComponent } from '../shared/components/breadcrumb/breadcrumb.component';
import {SlickCarouselModule} from 'ngx-slick-carousel';
import {FormsModule} from "@angular/forms";
import { ReactiveFormsModule } from '@angular/forms';
@NgModule({
  declarations: [
    BreadcrumbComponent
  ],
  imports: [
    RouterModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [BreadcrumbComponent, CommonModule, SlickCarouselModule, ReactiveFormsModule]
})
export class SharedModule { }
