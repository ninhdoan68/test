import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BreadcrumbComponent implements OnInit {
  @Input() arraySlugAdd: any;
  breadcrumbs;
  arraySlug = {
    '': 'Trang chủ',
    'gioi-thieu': 'Giới thiệu',
    'combo': 'Combo',
    'am-thuc': 'Ẩm thực',
    'thu-vien': 'Thư viện',
    'lien-he': 'Liên Hệ',
    'tim-kiem': 'Tìm kiếm',
    'dat-phong': 'Đặt phòng',
    'tin-tuc': 'Tin tức',
    'dich-vu': 'Dịch vụ',
    'to-chuc-su-kien': 'Tổ chức sự kiện',
    'khach-san': 'Khách sạn',
    'checkout': 'Thanh toán',
    'nha-san': 'Nhà sàn',
    'phong-hop': 'Phòng họp',
  };
  constructor(private Router: Router) {
  }

  createBreadcrumb(path) {
    let arrayPath = path.split('/');
    let breadcrumbs = [];

    let url = '';
    arrayPath.map(data => {
      if (data !== '') {
        url += '/' + data;
      }
      breadcrumbs.push({ text: this.arraySlug[data], routerLink: url });
    });
    return breadcrumbs;
  }
  ngOnInit() {
  }
  create(arraySlug, arraySlugAdd){
    this.arraySlug = Object.assign(arraySlug, arraySlugAdd);
    let breadcrumbs = this.createBreadcrumb(this.Router.url);
    return breadcrumbs;
  }

}
