import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';

import {HomeService} from '../../../shared/services/home.service';
import { BaseService } from '../../../shared/services/base.service';
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {

  showNotification = false;
  notification = 'Bạn đã đăng ký với email này';
  services = [
    { text: 'Combo', routerLink: '/combo' },
    { text: 'Đặt Phòng', routerLink: '/dat-phong' },
    { text: 'Ẩm Thực', routerLink: '/am-thuc' },
    { text: 'Tổ Chức Sự Kiện', routerLink: '/to-chuc-su-kien' },
    { text: 'Dịch Vụ', routerLink: '/dich-vu' },
  ];

  introduces = [
    { text: 'Tin Tức', routerLink: '/tin-tuc' },
    { text: 'Thư Viện', routerLink: '/thu-vien' },
    { text: 'Giới thiệu', routerLink: '/gioi-thieu' },
    { text: 'Liên Hệ', routerLink: '/lien-he' }
  ];
  Partners_footer;

  constructor(  private fb: FormBuilder,  private HomeService: HomeService, private BaseService: BaseService, ) {
    this.BaseService.getPartnerss().subscribe(data => {
      this.Partners_footer = data[0];
      // console.log('thissss', this.Partners_footer);
    });
  }

  sendEmail() {
    this.showNotification = true;
  }

  upToTop() {
    window.scrollTo(220, 0);
  }
  ContactForm;
  postContact() {
      this.HomeService.addCrmLead(this.ContactForm.value).subscribe();
    this.notification = "Cảm ơn bạn đã liên hệ với chúng tôi!";
    this.showNotification = true;
    this.ContactForm.reset();
  }
  ngOnInit() {
    this.ContactForm = this.fb.group({

      email: ['',
    Validators.email
  ],
      massage: [''],
    });
  }

}
