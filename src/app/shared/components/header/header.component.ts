import { Component, OnInit, ViewEncapsulation, HostListener } from '@angular/core';
import { HomeService } from '../../services/home.service';
import {BaseService} from '../../services/base.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  menus = [
    { text: 'Trang chủ', routerLink: '' },
    { text: 'Combo', routerLink: '/combo' },
    { text: 'Đặt Phòng', routerLink: '/dat-phong' },
    { text: 'Ẩm Thực', routerLink: '/am-thuc' },
    { text: 'Tổ Chức Sự Kiện', routerLink: '/to-chuc-su-kien' },
    { text: 'Dịch Vụ', routerLink: '/dich-vu' },
    { text: 'Tin Tức', routerLink: '/tin-tuc' },
    { text: 'Thư Viện', routerLink: '/thu-vien' },
    { text: 'Giới thiệu', routerLink: '/gioi-thieu' },
    { text: 'Liên Hệ', routerLink: '/lien-he' }
  ];
  showMenu = false;
  homelogo;
   logo ;
  Partners;
  constructor(private HomeService: HomeService, private BaseService: BaseService) {
    this.getHomeLoGo();
   }

  getHomeLoGo() {
    this.HomeService.getHomeLoGo().subscribe( data => {
      this.homelogo = data;
      this.homelogo.map(da => {
        if (Object.keys(da.irAttachments).length > 0) {
            da['image'] = this.BaseService.adminUrl + da.irAttachments[0].id + '/picture';
        } else {
          da['image'] = '';
        }
        });
        this.logo = this.homelogo.filter(logo => {
           return logo.slug == "home-logo";
        });
// console.log(this.homelogo);

    });
  }

  onResize(event) {}
  ngOnInit() {
    this.HomeService.getPartnerss().subscribe(data => {
      this.Partners = data;
    });
  }

}
