import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: './modules/home/home.module#HomeModule', pathMatch: 'full' },
  { path: 'gioi-thieu', loadChildren: './modules/introduce/introduce.module#IntroduceModule' },
  { path: 'lien-he', loadChildren: './modules/contact/contact.module#ContactModule' },
  { path: 'thu-vien', loadChildren: './modules/library/library.module#LibraryModule'},
  { path: 'combo', loadChildren: './modules/combo/combo.module#ComboModule'},
  { path: 'to-chuc-su-kien', loadChildren: './modules/event/event.module#EventModule'},
  { path: 'am-thuc', loadChildren: './modules/cuisine/cuisine.module#CuisineModule'},
  { path: 'dich-vu', loadChildren: './modules/dichvu/dichvu.module#DichvuModule'},
  { path: 'tin-tuc', loadChildren: './modules/tintuc/tintuc.module#TintucModule'},
  { path: 'dat-phong', loadChildren: './modules/booking/booking.module#BookingModule'},
  { path: 'tim-kiem', loadChildren: './modules/search/search.module#SearchModule'},
  { path: 'thank-you', loadChildren: './modules/thankyou/thankyou.module#ThankyouModule'},
  { path: '**', loadChildren: './modules/error404/error404.module#Error404Module'},
 

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
