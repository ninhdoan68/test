import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IntroduceRoutingModule } from './introduce-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    IntroduceRoutingModule
  ]
})
export class IntroduceModule { }
