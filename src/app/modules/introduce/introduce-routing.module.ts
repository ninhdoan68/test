import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IntroduceComponent } from './introduce/introduce.component';
import { SharedModule } from '../../shared/shared.module';

const routes: Routes = [
  { path: '', component: IntroduceComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes), SharedModule],
  exports: [RouterModule, IntroduceComponent],
  declarations: [IntroduceComponent]
})
export class IntroduceRoutingModule { }
