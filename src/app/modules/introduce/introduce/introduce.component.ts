import {Component, OnInit} from '@angular/core';
import {IntroduceService} from 'src/app/shared/services/introduce.service';
import {BaseService} from 'src/app/shared/services/base.service';
import {FormBuilder, Validators} from '@angular/forms';

@Component({
  selector: 'app-introduce',
  templateUrl: './introduce.component.html',
  styleUrls: ['./introduce.component.scss'],
})
export class IntroduceComponent implements OnInit {

  constructor(private fb: FormBuilder, private introduceService: IntroduceService, public baseService: BaseService) {
  }

  ContactForm;

  ngOnInit() {
    this.ContactForm = this.fb.group({
      name: ['',

        [
          Validators.required,
          Validators.minLength(0),
          Validators.maxLength(10000000000000),
          Validators.pattern('[a-zA-ZàáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹđÀÁẠẢÃÂẦẤẨẪẬĂẰẮẲẴẶÈÉẺẼẸÊỀẾỂỄỆÌÍỈĨỊÒÓỎÕỌÔỒỐỔỖỘƠỜỚỞỠỢÙÚỦŨỤƯỪỨỬỮỰỲÝỶỸỴĐ ]*')

        ]

      ],
      email: [
        '',
        [
          Validators.required,
          Validators.email,
        ]
      ],
      massage: ['']
    });
    this.getBanner();
    this.getAboutUs();
  }

  banner: any;
  banner_about: any;
  banner_contact: any;

  getBanner() {
    this.introduceService.getBanner().subscribe(data => {
      this.banner = data;
      console.log(this.banner);
      this.banner.map(data => {
        if ((data.irAttachments).length > 0) {
          data['image'] = this.baseService.adminUrl + data.irAttachments[0].id;
        }
        this.banner_about = this.banner.filter(dta => {
          return dta.locations_show === 'aboutus';
        });
        this.banner_contact = this.banner.filter(dta => {
          return dta.slug === 'banner-about-lienhe';
        });
      });
    });
  }

  us: any;
  aboutUs_1: any;
  aboutUs_2: any;

  getAboutUs() {
    this.introduceService.getAboutUs().subscribe(data => {
      this.us = data.filter(dta => {
        return (dta.name) !== "VỀ THIÊN SƠN SUỐI NGÀ ";
      });
      console.log(this.us);
      this.us.map(data => {
        if (Object.keys(data.irAttachments).length > 0) {
          data['image'] = this.baseService.adminUrl + data.irAttachments[0].id;
        }
        if (Object.keys(data.resGalleries).length > 0) {
          data.resGalleries.map(da => {

            if (Object.keys(da.resMedia).length > 0) {

              da.resMedia.map(item => {
                if (Object.keys(item.irAttachments).length > 0) {

                  item['image'] = this.baseService.adminUrl + item.irAttachments[0].id;
                  this.aboutUs_1 = this.us.filter(dta => {
                    return (dta.resGalleries).length > 0;
                  });
                  this.aboutUs_2 = this.us.filter(dta => {
                    return (dta.resGalleries).length === 0;
                  });
                }
              });

            }
          });
        }
      });
    });
  }

  postContact() {
    this.introduceService.getPartnerByEmail(this.ContactForm.value.email).subscribe(data => {
        console.log('data', data);
        if (data.length === 0) {
          this.introduceService.postContact(this.ContactForm.value).subscribe(da => {
            console.log('datappppp', da);
            const param = {
              'name': da.name,
              'email': da.email,
              'description': this.ContactForm.value.massage,
              'partner_id': da.id
            };
            // console.log('pa', param);
            this.introduceService.addCrmLead(param).subscribe();
            this.ContactForm.reset();
          });
        } else {

          const param = {
            'name': data[0].name,
            'email': data[0].email,
            'description': this.ContactForm.value.massage,
            'partner_id': data[0].id
          };
          this.introduceService.addCrmLead(param).subscribe(
            this.ContactForm.reset()
          );
        }
      }
    );
    alert('Cảm ơn bạn đã liên hệ với chúng tôi!');
  }
}
