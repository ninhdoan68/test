import {Component, OnInit} from '@angular/core';
import {ComboService} from "../../../shared/services/combo.service";
import {BaseService} from "../../../shared/services/base.service";
import {ActivatedRoute, Router} from '@angular/router';
import {EventService} from "../../../shared/services/event.service";

declare var $: any;

@Component({
  selector: 'app-combo-detail',
  templateUrl: './event-detail.component.html',
  styleUrls: ['./event-detail.component.scss']
})
export class EventDetailComponent implements OnInit {


  constructor(
    private eventService: EventService,
    private baseService: BaseService,
    public router: Router,
    public activatedRoute: ActivatedRoute
  ) {
    this.handleSlug();
  }

  arraySlug = {};
  slideConfigBlog1 = {
    'slidesToShow': 1, 'slidesToScroll': 1, 'arrows': true, 'asNavFor': '.carousel2', 'autoplay': true,
    'autoplaySpeed': 3500
  };
  slideConfigBlog2 = {
    'slidesToShow': 4,
    'slidesToScroll': 1,
    'asNavFor': '.carousel1',
    'dots': false,
    'autoplay': true,
    'focusOnSelect': true,
    'autoplaySpeed': 3500,
  };
  event_detail;
  galleryImages = [];
  num_people = 0;
  temp_numpeople = '';
  today = new Date();
  date = null;
  full_name = '';
  phone = '';
  email = '';
  service = [];
  allprice;
  validate_notice = {
    name: '',
    phone: '',
    email: '',
    num: '',
    date: ''
  };
  total_price = 0;
  check_char = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
  check_num = /[0123456789]+/;


  handleSlug() {
    const slug = this.activatedRoute.snapshot.paramMap.get('slug');
    this.eventService.getEventName(slug).subscribe(event_detail => {
      this.event_detail = event_detail[0];
      this.arraySlug[this.event_detail['slug']] = this.event_detail.name;
    });
  }

  ngOnInit() {
    this.handleParam();
  }

  handleParam() {
    const slug = this.activatedRoute.snapshot.paramMap.get('slug');
    this.eventService.getDetailEvent(slug).subscribe(event_detail => {
      this.event_detail = event_detail[0];
      if (this.event_detail.resGalleries.length) {
        this.event_detail.resGalleries.map(gall => {
          if (gall.resMedia.length) {
            gall.resMedia.map(media => {
              if (media.irAttachments.length) {
                media.irAttachments.map(img => {
                  const imageItem = {
                    image: this.baseService.adminUrl + img.id,
                  };
                  this.galleryImages.push(imageItem);
                });
              }
            });
          }
        });
      }
      this.eventService.getProductEvent(this.event_detail.resEventLines[0].id).subscribe(data => {
        this.event_detail.prod_id = data[0].id;
        const temp_data = {
          ser_id : data[0].id,
          name : this.event_detail.resEventLines[0].name,
          list_price : this.event_detail.resEventLines[0].price
        };
        this.service.push(temp_data);
      });
      // console.log(this.event_detail);
      if (event_detail[0].resAdditionalServices.length > 0) {
        this.event_detail.resAdditionalServices.map( service => {
          this.eventService.getProductservice(service.id).subscribe( data => {
            service['ser_id'] = data[0].id;
          });
        });
      }
    });
  }

  setnumber(bool) {
    this.num_people = (bool ? this.num_people + 1 : (this.num_people > 0 ? this.num_people - 1 : this.num_people));
    this.temp_numpeople = (this.num_people === 0 ? null : this.num_people + ' người');
  }

  set_service(data) {
    if (!this.eventService.checkExistsObjectInArrayObject(data, this.service, 'id')) {
      this.service.push(data);
    } else {
      this.service.splice(this.eventService.getIndexOfObjectInArray(data, this.service, 'id'), 1);
    }
    console.log('jjjj', this.service);
  }

  request_event() {
    this.service.forEach( item => {
      console.log('total', this.total_price);
      this.total_price = this.total_price + item.list_price;
    });
    this.validate_notice = {
      name: '',
      phone: '',
      email: '',
      num: '',
      date: ''
    };
    if (this.full_name.trim() === '') {
      this.validate_notice.name = 'notice_on';
      return;
    }
    if (this.check_num.test(this.full_name.trim()) || this.check_char.test(this.full_name.trim())) {
      this.validate_notice.name = 'notice_on2';
      return;
    }
    if (this.phone.trim() === '') {
      this.validate_notice.phone = 'notice_on';
      return;
    }
    if (isNaN(Number(this.phone)) || this.phone.trim().length !== 10) {
      this.validate_notice.phone = 'notice_on2';
      return;
    }
    if (this.email.trim() !== '' && !this.eventService.isEmail(this.email.trim())) {
      this.validate_notice.email = 'notice_on2';
      return;
    }
    if (this.num_people === 0) {
      this.validate_notice.num = 'notice_on';
      return;
    }
    if (this.date === null) {
      this.validate_notice.date = 'notice_on';
      return;
    }
    this.eventService.getPartnerByMobile(this.phone).subscribe(
      partner => {
        console.log('infor_partner', partner);
        if (partner.length === 0) {
          const data_request_partner = {
            create_date: this.today,
            name: this.full_name,
            display_name: this.full_name,
            email: this.email,
            mobile: this.phone,
            invoice_warn: "no-message",
            sale_warn: "no-message",
            picking_warn: "no-message"
          };
          this.eventService.send_request_partner(data_request_partner).subscribe(response_partner => {
            console.log('response_partner', response_partner);
            this.processPartner(response_partner);
          });
        } else {
          this.processPartner(partner[0]);
        }
      });
  }

  processPartner(partner) {
    this.eventService.getSaleOrder().subscribe(a => {
      this.service.map( item4 => {
        this.allprice = this.allprice + item4.list_price;
      });
      const b = a[0].id + 1;
      const date = new Date();
      const paramsSale = {
        name: 'SO0' + b,
        department_id: 2,
        date_order: date.toISOString(),
        partner_id: partner.id,
        state: 'draft',
        note: 'Số người: ' + this.num_people + '\n Ngày tổ chức:' + this.date.toLocaleString(),
        partner_shipping_id: partner.id,
        partner_invoice_id: partner.id,
        pricelist_id: 1,
        team_id: 1,
        warehouse_id: 1,
        picking_policy: 'direct',
        amount_total: this.total_price
      };
      this.eventService.saveSaleOrder(paramsSale).subscribe(saleResult => {
        console.log('sale order ok ');
        this.eventService.getHotelFolio().subscribe(a => {
          const b = a[0].id + 1;
          const paramsHotelFolio = {
            name: 'Folio/00' + b,
            order_id: saleResult.id,
            checkin_date: this.today.toISOString(),
            checkout_date: this.today.toISOString(),
            hotel_policy: "manual",
            duration: 1,
            state: 'draft',
            notes: 'Số người: ' + this.num_people + '\n Ngày tổ chức:' + this.date.toLocaleString(),
            amount_total: this.allprice,
            amount_untaxed: this.allprice
          };
          this.eventService.saveHotelFolio(paramsHotelFolio).subscribe(HotelFolioResult => {
            console.log('Folio ok ');
            this.service.forEach( item => {
              this.eventService.saveSaleOrderLine(
                {
                  customer_lead: 0,
                  name: '[' + item.name + '] - ' + this.date.toLocaleString(),
                  order_id: saleResult.id,
                  product_id: item.ser_id,
                  product_uom: 29,
                  price_unit: item.list_price,
                  product_uom_qty: 1,
                  price_subtotal: item.list_price
                }
              ).subscribe(saleLineResult => {
                console.log(saleLineResult);
                const paramServiceLine = {
                  service_line_id: saleLineResult.id,
                  folio_id: HotelFolioResult.id,
                  ser_checkin_date: this.date.toISOString(),
                  ser_checkout_date: this.date.toISOString()
                };
                console.log(paramServiceLine);
                this.eventService.saveHotelServiceLine(paramServiceLine).subscribe(res => {
                  console.log('okok');
                  this.router.navigate(['/thank-you']);
                });
              });
            });
          });
        });
      });
    });
  }
}
