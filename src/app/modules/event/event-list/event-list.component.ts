import { Component, OnInit} from '@angular/core';
import {EventService} from "../../../shared/services/event.service";
import {BaseService} from "../../../shared/services/base.service";

@Component({
  selector: 'app-combo-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.scss']
})
export class EventListComponent implements OnInit {
  constructor(
    private eventService: EventService,
    private baseService: BaseService
  ) {}

  list_allevents = [];
  ngOnInit() {
    this.handleParam();
  }
  handleParam() {
    this.eventService.getEvent().subscribe( events => {
      events.forEach( item_event => {
        if (Object.keys(item_event).length > 0 && typeof item_event !== 'undefined' && item_event.irAttachments.length > 0) {
          item_event.image = this.baseService.adminUrl + item_event.irAttachments[0].id + '/picture';
        } else {
          item_event.image = '../assets/image/no-image.png';
        }
      });
      this.list_allevents = events;
      // console.log('adsasd', this.list_allevents);
    });
  }

}
