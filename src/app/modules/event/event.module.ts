import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventRoutingModule } from './event-routing.module';
import {EventListComponent} from './event-list/event-list.component';
import {EventDetailComponent} from './event-detail/event-detail.component';
import {SharedModule} from "../../shared/shared.module";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {BsDatepickerModule, BsDropdownModule} from "ngx-bootstrap";
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [EventListComponent, EventDetailComponent],
  imports: [CommonModule, EventRoutingModule, SharedModule, NgbModule, BsDropdownModule.forRoot(), FormsModule, BsDatepickerModule.forRoot()]
})
export class EventModule {}
