import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactComponent } from './contact/contact.component';
import { SharedModule } from '../../shared/shared.module';

const routes: Routes = [
  { path: '', component: ContactComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes), SharedModule],
  exports: [RouterModule, ContactComponent],
  declarations: [ContactComponent]
})
export class ContactRoutingModule { }
