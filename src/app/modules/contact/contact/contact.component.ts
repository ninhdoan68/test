import {
  Component,
  OnInit,
  ViewEncapsulation,
  ChangeDetectionStrategy
} from '@angular/core';
import {
  FormBuilder
} from '@angular/forms';
import {
  Validators
} from '@angular/forms';
import {
  ContactService
} from '../../../shared/services/contact.service';
import {
  BaseService
} from '../../../shared/services/base.service';
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ContactComponent implements OnInit {
  ContactForm;
  constructor(private fb: FormBuilder, private cs: ContactService, private bs: BaseService) {}
  contact;
  getAllContact() {
    this.cs.getContact().subscribe(data => {
      data.map(da => {
        da.website = da.website.replace('http:// ', '');
        this.contact = da;
      })

      console.log(this.contact);

    })
  }
  banner;
  ImgUrl = this.bs.adminUrl;
  Partners_footer;
  getAllBanner() {
    this.cs.getBanner().subscribe(data => {
      data.map(da => {
        da.irAttachments.map(d => {
          da["BANNER"] = `${this.ImgUrl}${d.id}`;
        })
        this.banner = da;
      })
      console.log(this.banner);
    })
  }
  postContact() {
    // this.cs.postContact(this.ContactForm.value).subscribe(data => {
    //   console.log('data', data);
    //   this.cs.addCrmLead(data).subscribe();
    // });
    this.cs.getPartnerByEmail(this.ContactForm.value.email).subscribe(data => {
      console.log('data', data);
      if (data.length === 0) {
        this.cs.postContact(this.ContactForm.value).subscribe(da => {
          console.log('data', da);
          const param = {
            'name': da.name,
            'email': da.email,
            'description': this.ContactForm.value.massage,
            'partner_id': da.id
          };
          this.cs.addCrmLead(param).subscribe();
          this.ContactForm.reset();
        })
      }
      else {
        console.log('mesage', this.ContactForm.value.massage);
        const param = {
          "name": data[0].name,
          "email": data[0].email,
          "description": this.ContactForm.value.massage,
          "partner_id": data[0].id
        };
        this.cs.addCrmLead(param).subscribe();
        this.ContactForm.reset();
      }
    }
    );
    alert("Cảm ơn bạn đã liên hệ với chúng tôi!");
  }
  ngOnInit() {
    this.bs.getPartnerss().subscribe(data => {
      this.Partners_footer = data[0];
      // console.log('thissss', this.Partners_footer);
    });
    this.ContactForm = this.fb.group({
      name: ['',

        [
          Validators.required,
          // Validators.minLength(4),
          // Validators.maxLength(35),
          Validators.pattern("[a-zA-ZàáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹđÀÁẠẢÃÂẦẤẨẪẬĂẰẮẲẴẶÈÉẺẼẸÊỀẾỂỄỆÌÍỈĨỊÒÓỎÕỌÔỒỐỔỖỘƠỜỚỞỠỢÙÚỦŨỤƯỪỨỬỮỰỲÝỶỸỴĐ ]*")

        ]

      ],
      email: [
        '',
        [
          Validators.required,
          Validators.email,
        ]
      ],
      massage: ['',
      ],
    })


    this.getAllContact();
    this.getAllBanner();
  }

}
