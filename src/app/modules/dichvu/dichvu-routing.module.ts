import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DichvuComponent } from './dichvu/dichvu.component';

const routes: Routes = [
{
  path: '',
  component: DichvuComponent
}
];
@NgModule({
  imports: [RouterModule.forChild(routes),],
  exports: [RouterModule],
  declarations: []
})
export class DichvuRoutingModule { }
