import { Component, OnInit } from '@angular/core';
import { DichvuService } from '../../../shared/services/dichvu.service';
import {BaseService} from '../../../shared/services/base.service';
@Component({
  selector: 'app-dichvu',
  templateUrl: './dichvu.component.html',
  styleUrls: ['./dichvu.component.scss']
})
export class DichvuComponent implements OnInit {

  constructor(private  DichvuService:DichvuService , private BaseService:BaseService) { 
    this.getDichvu();
  }
  dichvu;
  dich_vu_giai_tri;
  dich_vu_khac;
  getDichvu(){
    this.DichvuService.getDichvu().subscribe(data=>{
      this.dichvu=data;
      console.log(this.dichvu);
      this.dichvu.map(data => {      
            if(typeof data.irAttachments[0] !== 'undefined'){
              data['image']=this.BaseService.adminUrl+data.irAttachments[0].id+'/picture';
          }
          else{
            data['image']='../assets/image/no-image.png';
          }
     
        
      this.dich_vu_giai_tri=this.dichvu.filter(da=>{
        return da.service_type=="dịch vụ vui chơi giải trí";
      });
      this.dich_vu_khac=this.dichvu.filter(da=>{
        return da.service_type=="dịch vụ khác";
      });
      // console.log('giai tri',this.dich_vu_giai_tri);
      // console.log(this.dich_vu_khac);
      
      });
    })

  }
  ngOnInit() {
  }

}
