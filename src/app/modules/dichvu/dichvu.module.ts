import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DichvuComponent} from '../../modules/dichvu/dichvu/dichvu.component';
import { DichvuRoutingModule } from './dichvu-routing.module';


@NgModule({
  declarations: [DichvuComponent],
  imports: [
    CommonModule,
    DichvuRoutingModule,
    
  ],
  exports: []
})
export class DichvuModule { }
