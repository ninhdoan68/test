import { Component, OnInit} from '@angular/core';
import {ComboService} from '../../../shared/services/combo.service';
import {BaseService} from '../../../shared/services/base.service';

@Component({
  selector: 'app-combo-list',
  templateUrl: './combo-list.component.html',
  styleUrls: ['./combo-list.component.scss']
})
export class ComboListComponent implements OnInit {
  constructor(
    private comboService: ComboService,
    private baseService: BaseService
  ) {}
  ngOnInit() {
  }
}
