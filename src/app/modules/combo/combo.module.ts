import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ComboRoutingModule } from './combo-routing.module';
import { ComboListComponent } from './combo-list/combo-list.component';
import { ComboDetailComponent } from './combo-detail/combo-detail.component';
import {SharedModule} from "../../shared/shared.module";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {BsDatepickerModule, BsDropdownModule} from "ngx-bootstrap";

@NgModule({
  declarations: [ComboListComponent, ComboDetailComponent],
  imports: [CommonModule, ComboRoutingModule, SharedModule, NgbModule, BsDropdownModule.forRoot(), FormsModule, BsDatepickerModule.forRoot()]
})
export class ComboModule {}
