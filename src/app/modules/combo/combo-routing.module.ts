import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ComboDetailComponent } from './combo-detail/combo-detail.component';
import { ComboListComponent } from './combo-list/combo-list.component';
import {SharedModule} from "../../shared/shared.module";

const routes: Routes = [
  {
    path: '',
    component: ComboListComponent
  },
  {
    path: ':slug',
    component: ComboDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes), SharedModule],
  exports: [RouterModule]
})
export class ComboRoutingModule {}
