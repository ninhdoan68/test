import {Component, OnInit} from '@angular/core';
import {ComboService} from "../../../shared/services/combo.service";
import {BaseService} from "../../../shared/services/base.service";
import {ActivatedRoute,Router} from "@angular/router";

declare var $: any;

@Component({
  selector: 'app-combo-detail',
  templateUrl: './combo-detail.component.html',
  styleUrls: ['./combo-detail.component.scss']
})
export class ComboDetailComponent implements OnInit {


  constructor(
    private comboService: ComboService,
    private baseService: BaseService,
    public activatedRoute: ActivatedRoute,
    public router: Router
  ) {


    this.handleSlug();
  }

  arraySlug = {};
  slideConfigBlog1 = {
    'slidesToShow': 1, 'slidesToScroll': 1, 'arrows': true, 'asNavFor': '.carousel2', 'autoplay': true,
    'autoplaySpeed': 2500
  };
  slideConfigBlog2 = {
    'slidesToShow': 4,
    'slidesToScroll': 1,
    'asNavFor': '.carousel1',
    'dots': false,
    'autoplay': true,
    'focusOnSelect': true,
    'autoplaySpeed': 2500
  };
  galleryImages = [];
  combo_detail;
  num_people = 0;
  temp_numpeople = '';
  today = new Date();
  date = null;
  full_name = '';
  phone = '';
  email = '';
  notes = '';
  validate_notice = {
    name: '',
    phone: '',
    num: '',
    email: '',
    date: ''
  };
  check_char = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?`~]+/;
  check_num = /[0123456789]+/;

  async ngOnInit() {
    this.handleParam();
  }

  handleSlug() {
    const slug = this.activatedRoute.snapshot.paramMap.get('slug');
    this.comboService.getComboName(slug).subscribe(combo_detail => {
      this.combo_detail = combo_detail[0];
      this.arraySlug[this.combo_detail.slug] = this.combo_detail.name;
    });
  }

  handleParam() {
    this.activatedRoute.params.subscribe(combo => {
      const slug = this.activatedRoute.snapshot.paramMap.get('slug');
      this.comboService.getDetailCombo(slug).subscribe(combo_detail => {
        this.combo_detail = combo_detail[0];
        if (this.combo_detail.resGalleries.length) {
          this.combo_detail.resGalleries.map(gall => {
            if (gall.resMedia.length) {
              gall.resMedia.map(media => {
                if (media.irAttachments.length) {
                  media.irAttachments.map(img => {
                    const imageItem = {
                      image: this.baseService.adminUrl + img.id,
                    };
                    this.galleryImages.push(imageItem);
                  });
                }
              });
            }
          });
        }
        if (this.combo_detail.resPromotions.length) {
          if (this.combo_detail.resPromotions[0].discount_type === '%') {
            this.combo_detail.promotion_price = this.combo_detail.price - (this.combo_detail.price * this.combo_detail.resPromotions[0].discount_value / 100);
          } else {
            this.combo_detail.promotion_price = this.combo_detail.price - this.combo_detail.resPromotions[0].discount_value;
          }
        } else {
          this.combo_detail.promotion_price = this.combo_detail.price;
        }
        this.comboService.getProductCombo(this.combo_detail.id).subscribe(data => {
          if (data) {
            this.combo_detail.prod_id = data[0].id;
          }
        });
      });
    });
  }

  setnumber(bool) {
    this.num_people = (bool ? this.num_people + 1 : (this.num_people > 0 ? this.num_people - 1 : this.num_people));
    this.temp_numpeople = (this.num_people === 0 ? null : this.num_people + ' người');
  }

  request_combo() {
    this.validate_notice = {
      name: '',
      phone: '',
      email: '',
      num: '',
      date: ''
    };
    if (this.full_name.trim() === '') {
      this.validate_notice.name = 'notice_on';
      return;
    }
    if (this.check_num.test(this.full_name.trim()) || this.check_char.test(this.full_name.trim())) {
      this.validate_notice.name = 'notice_on2';
      return;
    }
    if (this.phone.trim() === '') {
      this.validate_notice.phone = 'notice_on';
      return;
    }
    if (isNaN(Number(this.phone)) || this.phone.trim().length !== 10) {
      this.validate_notice.phone = 'notice_on2';
      return;
    }
    if (this.email.trim() !== '' && !this.comboService.isEmail(this.email.trim())) {
      this.validate_notice.email = 'notice_on2';
      return;
    }
    if (this.num_people === 0) {
      this.validate_notice.num = 'notice_on';
      return;
    }
    if (this.date === null) {
      this.validate_notice.date = 'notice_on';
      return;
    }
    this.comboService.getPartnerByMobile(this.phone).subscribe(
      partner => {
        console.log('infor_partner', partner);
        if (partner.length === 0) {
          const data_request_partner = {
            create_date: this.date,
            name: this.full_name,
            display_name: this.full_name,
            email: this.email,
            mobile: this.phone,
            invoice_warn: "no-message",
            sale_warn: "no-message",
            picking_warn: "no-message"
          };
          this.comboService.send_request_partner(data_request_partner).subscribe(response_partner => {
            console.log('response_partner', response_partner);
            this.baseService.updatePartner(response_partner).subscribe(rateupdate => {
              console.log('rateupdate', rateupdate);
            });
            this.processPartner(response_partner);
          });
        } else {
          this.processPartner(partner[0]);
        }
      });
  }

  processPartner(partner) {
    this.comboService.getSaleOrder().subscribe(a => {
      const b = a[0].id + 1;
      const date = new Date();
      const paramsSale = {
        name: 'SO0' + b,
        department_id: 2,
        date_order: date.toISOString(),
        partner_id: partner.id,
        state: 'draft',
        note: ' Số người: ' + this.num_people + '\n Ngày nhận phòng:' + this.date.toLocaleString() + '\n Yêu cầu thêm: ' + this.notes,
        partner_shipping_id: partner.id,
        partner_invoice_id: partner.id,
        pricelist_id: 1,
        team_id: 1,
        warehouse_id: 1,
        picking_policy: 'direct',
        amount_total: (this.combo_detail.promotion_price !== null ||
          this.combo_detail.resPromotions.length > 0) ? this.combo_detail.promotion_price : this.combo_detail.price
      };
      this.comboService.saveSaleOrder(paramsSale).subscribe(saleResult => {
        console.log('sale order ok ');
        this.comboService.getHotelFolio().subscribe(a => {
          const b = a[0].id + 1;
          const paramsHotelFolio = {
            name: 'Folio/00' + b,
            order_id: saleResult.id,
            checkin_date: this.today.toISOString(),
            checkout_date: this.today.toISOString(),
            hotel_policy: "manual",
            note: ' Số người: ' + this.num_people + '\n Ngày nhận phòng:' + this.date.toLocaleString() + '\n Yêu cầu thêm: ' + this.notes,
            duration: 1,
            state: 'draft'
          };
          this.comboService.saveHotelFolio(paramsHotelFolio).subscribe(HotelFolioResult => {
            console.log('Folio ok ');
            this.comboService.saveSaleOrderLine(
                {
                customer_lead: 0,
                name: '[' + this.combo_detail.name + '] - ' + this.date.toLocaleString(),
                order_id: saleResult.id,
                product_id: this.combo_detail.prod_id,
                product_uom: 29,
                price_unit: (this.combo_detail.promotion_price !== null ||
                  this.combo_detail.resPromotions.length > 0) ? this.combo_detail.promotion_price : this.combo_detail.price,
                product_uom_qty: 1,
                price_subtotal: (this.combo_detail.promotion_price !== null ||
                  this.combo_detail.resPromotions.length > 0) ? this.combo_detail.promotion_price : this.combo_detail.price
              }
            ).subscribe(saleLineResult => {
              console.log(saleLineResult);
              const paramServiceLine = {
                service_line_id: saleLineResult.id,
                folio_id: HotelFolioResult.id,
                ser_checkin_date: this.date.toISOString(),
                ser_checkout_date: this.date.toISOString()
              };
              console.log(paramServiceLine);
              this.comboService.saveHotelServiceLine(paramServiceLine).subscribe(res => {
                console.log('okok');
                this.router.navigate(['/thank-you']);
              });
            });
          });
        });
      });
    });
  }
}
