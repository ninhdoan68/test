import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TintucService} from '../../../shared/services/tintuc.service';
import {BaseService} from '../../../shared/services/base.service';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-tintuc-detail',
  templateUrl: './tintuc-detail.component.html',
  styleUrls: ['./tintuc-detail.component.scss'],
  // encapsulation: ViewEncapsulation.Native
})
export class TintucDetailComponent implements OnInit {
  slug_cate;

  constructor(private TintucService: TintucService, private BaseService: BaseService, private ActivatedRoute: ActivatedRoute, private httpClient: HttpClient, ) {
    let slug = this.ActivatedRoute.snapshot.paramMap.get('slug');
    let slug_cate = this.ActivatedRoute.snapshot.paramMap.get('slug_cate');
    this.slug_cate = slug_cate;
    this.getBlogcategoriesslug(slug);
    this.httpClient.get<{ ip: string }>('https://jsonip.com')
      .subscribe(data => {
        console.log('IP: ', data.ip);
        this.ipAddress = data.ip;
      });
    // this.getBlogcategories();


  }

  //    srcHeadFacebook = "https://www.facebook.com/plugins/like.php?href=";
  // scrFootFacebook = "&width=191&layout=button_count&action=like&size=large&show_faces=true&share=true&height=28&appId";
  // trust_srcFacebook;
  // srcFacebook;
  ipAddress: any;
  imgPath;
  tintuc;
  slug;
  h1;
  list_cmt: any;
  validate_rating_notice = {
    name : '',
    emailorphone : ''
  };
  full_name_rate = '';
  email_rate = '';
  phone_rate = '';
  email_phone_rate = '';
  note_rate = '';
  check_char = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
  check_num = /[0123456789]+/;
  checkismail = /@/;

  getTableOfContent(tag, tagRemove, string) {
    let tableOfContent = [];

    let array1 = string.split('<' + tag);

    array1.map(data => {
      let array2 = data.split('</' + tag + '>');
      if (array2.length > 1) {
        let check = true;
        for (let key of Object.keys(tagRemove)) {
          let value = tagRemove[key];
          if (array2[0].replace('<' + value, '') !== array2[0]) {
            check = false;
            break;
          }
        }
        if (check === true) {
          tableOfContent.push('<' + tag + ' ' + array2[0] + '</' + tag + '>');
        }
      }
    });
    return tableOfContent;
  }

  getBlogcategoriesslug(slug) {

    this.TintucService.getBlogbySlug(slug).subscribe(data => {

      this.tintuc = data;

      this.getRating(this.tintuc[0].id);
      this.TintucService.getListCommentByBlog(this.tintuc[0].id).subscribe( list_comment => {
        this.list_cmt = list_comment;
        console.log('cmt ', this.list_cmt);
      });
      this.getBlogcategories(slug);

      if (data.length > 0) {
        this.h1 = this.getTableOfContent('h2', ['img', 'table', 'input'], data[0].description);
        if (sessionStorage.getItem('views')) {
          let views = JSON.parse(sessionStorage.getItem('views'));
          // console.log(views);
          if (views.filter(view => view === this.slug).length === 0) {
            data[0].view += 1;
            this.TintucService.updateData(data[0]).subscribe(da => {
              // console.log(da);
            });
            views.push(this.slug);
            sessionStorage.setItem('views', JSON.stringify(views));
          }


        } else {
          data[0].view += 1;
          this.TintucService.updateData(data[0]).subscribe();
          let views = [];
          views.push(this.slug);
          sessionStorage.setItem('views', JSON.stringify(views));
        }
      }


      this.tintuc.map(data => {

        if (Object.keys(data.irAttachments).length > 0) {
          data['image'] = this.BaseService.adminUrl + data.irAttachments[0].id + '/picture';
        } else {
          data['image'] = '';
        }
      });

    });


  }

  blogs_lien_quan = [];
  tintucnoibat;
  noibat;
  tinkhac = [];
  sukien;
  chuyenmuc;
  tag;

  getBlogcategories(slug) {
    this.TintucService.getBlogcategories(this.slug_cate).subscribe(data => {

      this.noibat = data;
      this.noibat.map(data => {
        if (Object.keys(data.resBlogs).length > 0) {
          data.resBlogs.map(da => {
            if (Object.keys(da.irAttachments).length > 0) {
              da['image'] = this.BaseService.adminUrl + da.irAttachments[0].id + '/picture';
            } else {
              da['image'] = '';
            }
          });
        } else {
          data['img'] = 'No_image_avalilable.svg';
        }

        this.tinkhac = this.noibat.filter(da => {
          da.resBlogs = da.resBlogs.filter(data => {
            return data.slug !== slug;
          });
          return da.slug === this.slug_cate;
        });
        // console.log(this.noibat);
        this.chuyenmuc = this.noibat.filter(da => {
          da.resBlogs = da.resBlogs.filter(data => {
            return data.slug !== slug;
          });
          return da.slug === this.slug_cate;
        });
        // console.log(this.chuyenmuc);
        // console.log(this.tinkhac);


      });


    });
  }

  // -------------RATING--------------

  res_rate_urls;
  rate;
  rate_avg;

  topRating() {
    let rate = {
      id_post: this.tintuc[0].id,
      ip: this.ipAddress,
      rate: this.rate
    };
    console.log('data', rate);
    this.TintucService.checkRateExit(rate).subscribe(response => {
      console.log('data111', response);
      // if (Object.keys(response).length > 0) {
      //   alert('Bạn đã chấm bài viết này ' + rate.rate + ' SAO rồi!');
      // } else {
        this.TintucService.checkRatedExit(rate).subscribe(rated => {
          console.log('data122', rated);
          if (Object.keys(rated).length > 0) {
            rated.map(item => {
              let rate_update = {
                id: item.id,
                rate: this.rate
              };
              console.log('data333', rate_update);
              this.TintucService.updateRate(rate_update).subscribe(rateupdate => {
                alert('Cảm ơn bạn đã cập nhật đánh giá!');
                this.getRating(this.tintuc[0].id);
              });
            });
          } else {
            this.TintucService.addRate(rate).subscribe(data => {
              if (Object.keys(data).length > 0) {
                this.getRating(this.tintuc[0].id);
                alert('Cảm ơn bạn đã đánh giá!');
              }
            });
          }
        });
      // }
    });
  }

  getRating(id) {
    this.TintucService.countRateByPost(id).subscribe(data => {
      // console.log('RATING', data);
      if (data.length === 0) {
        this.rate = 0;
      } else {
        let totalRate = 0;
        data.map(da => {
          totalRate += parseInt((da.rate ? da.rate : 0), 10);
        });
        this.rate_avg = Math.ceil(totalRate / data.length);
        this.rate = this.rate_avg;

      }
    });
  }

  ngOnInit() {
  }

  ngOnChanges() {
    // this.Jquery();
  }

  // Jquery() {
  //   (function (d, s, id) {
  //     let js, fjs = d.getElementsByTagName(s)[0];
  //     // if (d.getElementById(id)) {
  //     //   return;
  //     // }
  //     js = d.createElement(s);
  //     js.id = id;
  //     js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.2&appId=421249515109247&autoLogAppEvents=1';
  //     fjs.parentNode.insertBefore(js, fjs);
  //
  //     if (d.getElementById(id)) {
  //       delete (<any>window).FB;
  //       fjs.parentNode.replaceChild(js, fjs);
  //     } else {
  //       fjs.parentNode.insertBefore(js, fjs);
  //     }
  //
  //   }(document, 'script', 'facebook-jssdk'));
  //   let url = window.location.href;
  //   url = url.replace("http://localhost:4200", "http://hanoitourist.azsolutions.vn");
  //   this.urlCurrent = url;
  //   this.srcFacebook = this.srcHeadFacebook + encodeURI(url) + this.scrFootFacebook;
  //   this.trust_srcFacebook = this.sanitizer.bypassSecurityTrustResourceUrl(this.srcFacebook);
  // }

  send_request_rate() {
    if (this.checkismail.test(this.email_phone_rate.trim())) {
      this.email_rate = this.email_phone_rate;
    } else {
      this.phone_rate = this.email_phone_rate;
    }
    this.validate_rating_notice = {
      name : '',
      emailorphone : ''
    };
    if (this.full_name_rate.trim() === '') {
      this.validate_rating_notice.name = 'notice_on';
      return;
    }
    if (this.check_num.test(this.full_name_rate.trim()) || this.check_char.test(this.full_name_rate.trim())) {
      this.validate_rating_notice.name = 'notice_on2';
      return;
    }
    if (this.email_phone_rate.trim() === '') {
      this.validate_rating_notice.emailorphone = 'notice_on';
      return;
    }
    if (this.checkismail.test(this.email_phone_rate.trim())) {
      if (!this.TintucService.isEmail(this.email_phone_rate.trim())) {
        this.validate_rating_notice.emailorphone = 'notice_on2';
        return;
      }
    } else {
      if (isNaN(Number(this.email_phone_rate)) || this.email_phone_rate.trim().length !== 10) {
        this.validate_rating_notice.emailorphone = 'notice_on2';
        return;
      }
    }
    // alert('ok');

    const data_send_request_rate = {
      name: this.full_name_rate,
      email: this.email_rate,
      phone: this.phone_rate,
      title_comment: '',
      comment: this.note_rate,
      model: 'res.blog',
      obj_id: this.tintuc[0]['id'],
      clean: 0,
      employees: 0,
      food: 0,
      location: 0,
      price_room: 0,
      quality: 0
    };
    this.TintucService.send_request_rate(data_send_request_rate).subscribe(response => {
      this.full_name_rate = '';
      this.email_rate = '';
      this.phone_rate = '';
      // this.rate = '';
      // this.notification = 'Cảm ơn bạn đã đánh giá!';
      // this.showNotification = true;
      alert(response);
    });
  }


}
