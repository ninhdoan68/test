import { Component, OnInit } from '@angular/core';
import { TintucService } from '../../../shared/services/tintuc.service';
import {BaseService} from '../../../shared/services/base.service';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-tintuc-noibat',
  templateUrl: './tintuc-noibat.component.html',
  styleUrls: ['./tintuc-noibat.component.scss']
})
export class TintucNoibatComponent implements OnInit {

  constructor(private TintucService:TintucService ,private BaseService:BaseService,private ActivatedRoute:ActivatedRoute ) { 
    // let slug = this.ActivatedRoute.snapshot.paramMap.get('slug');
    let slug_cate = this.ActivatedRoute.snapshot.paramMap.get('slug_cate');
    this.getBlogcategories(slug_cate);
    this.updateViewBlog(this.id_blog);
    this.getBlogcategoriess();
  }
  p = 1;
  id_blog;
  tintuc;
  tintucnoibat;
  noibat;
  tinkhac=[];
  sukien;
  getBlogcategories(slug_cate){
    this.TintucService.getBlogcategories(slug_cate).subscribe(data=>{
this.tintuc=data;
this.tintuc.map(data => {      
  if (Object.keys(data.resBlogs).length > 0) {
    data.resBlogs.map(da => {
      if(Object.keys(da.irAttachments).length >0){
        da['image']=this.BaseService.adminUrl+da.irAttachments[0].id+'/picture';
    }
    else{
      da['image']='';
    }


    });
  }
  else {
    data['img'] ='No_image_avalilable.svg';
  }
// this.tintucnoibat=this.tintuc.filter(da=>{
//   return da.slug=="tin-tuc-noi-bat";
// });
this.tinkhac=this.tintuc.filter(da=>{
  return da.slug !=slug_cate;
});

console.log(this.tintuc); 
console.log(this.tinkhac); 
});
  
    });
  }
  view;
  slug;
  updateViewBlog(id_blog) {
    if (sessionStorage.getItem('view')) {
      let string = sessionStorage.getItem('view');
      let array = string.split(',');
      for (let k in array) {
        let v = array[k];
        let string = v.replace('["', '');
        string = string.replace('"]', '');
        string = string.replace('"', '');
        string = string.replace('"', '');
        array[k] = string;
      }
      if (array.includes(this.slug) == true) {
      } else {
        array.push(this.slug);
        sessionStorage.setItem('view', JSON.stringify(array));
        // xu ly + view
        let params = {
          id: id_blog,
          view: this.view + 1
        }
        this.TintucService.updateViewBlog(params).subscribe(response => console.log(response))
      }
    } else {
      let array = [];
      array.push(this.slug);
      sessionStorage.setItem('view', JSON.stringify(array));
      let params = {
        id: id_blog,
        view: this.view + 1
      }
      this.TintucService.updateViewBlog(params).subscribe(response => console.log(response))
    }
  }

  tinmoi;
  getBlogcategoriess(){
    this.TintucService.getBlogmoinhat().subscribe(data=>{
this.tinmoi=data;
console.log('tinmoi',this.tinmoi);

this.tinmoi.map(da => {      

      if(Object.keys(da.irAttachments).length >0){
        da['image']=this.BaseService.adminUrl+da.irAttachments[0].id+'/picture';
    }
    else{
      da['image']='';
    }



});
  
    });
  }
  
  ngOnInit() {
  }

}
