
import { Component, OnInit, ViewEncapsulation,  } from '@angular/core';
import { TintucService } from '../../../shared/services/tintuc.service';
import {BaseService} from '../../../shared/services/base.service';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-tintuc',
  templateUrl: './tintuc.component.html',
  styleUrls: ['./tintuc.component.scss']
})
export class TintucComponent implements OnInit {

  constructor(private TintucService:TintucService ,private BaseService:BaseService,private ActivatedRoute:ActivatedRoute ) { 
    // let slug = this.ActivatedRoute.snapshot.paramMap.get('slug');
    this.getBlogcategories();
  }
  tintuc;
  tintucnoibat;
  noibat;
  tinkhac=[];
  sukien;
  getBlogcategories(){
    this.TintucService.getBlogcategoriesAll().subscribe(data=>{
this.tintuc=data;
console.log('tintuc',this.tintuc);

this.tintuc.map(data => {      
  if (Object.keys(data.resBlogs).length > 0) {
    data.resBlogs.map(da => {
      if(Object.keys(da.irAttachments).length >0){
        da['image']=this.BaseService.adminUrl+da.irAttachments[0].id+'/picture';
    }
    else{
      da['image']='';
    }


    });
  }
  else {
    data['img'] ='No_image_avalilable.svg';
  }


});
  
    });
  }
  
  slug;
  view;
  updateViewBlog(id_blog) {
    if (sessionStorage.getItem('view')) {
      let string = sessionStorage.getItem('view');
      let array = string.split(',');
      for (let k in array) {
        let v = array[k];
        let string = v.replace('["', '');
        string = string.replace('"]', '');
        string = string.replace('"', '');
        string = string.replace('"', '');
        array[k] = string;
      }
      if (array.includes(this.slug) == true) {
      } else {
        array.push(this.slug);
        sessionStorage.setItem('view', JSON.stringify(array));
        // xu ly + view
        let params = {
          id: id_blog,
          view: this.view + 1
        }
        this.TintucService.updateViewBlog(params).subscribe(response => console.log(response))
      }
    } else {
      let array = [];
      array.push(this.slug);
      sessionStorage.setItem('view', JSON.stringify(array));
      let params = {
        id: id_blog,
        view: this.view + 1
      }
      this.TintucService.updateViewBlog(params).subscribe(response => console.log(response))
    }
    console.log(this.view);
    
  }

  ngOnInit() {
  
  }

}
