import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TintucComponent } from './tintuc/tintuc.component';
import { TintucNoibatComponent } from './tintuc-noibat/tintuc-noibat.component';
import { TintucDetailComponent } from './tintuc-detail/tintuc-detail.component';

const routes: Routes = [
  {
    path: '',
    component: TintucComponent,

  },
   {
    path: ':slug_cate',
    component: TintucNoibatComponent,
    
  },

  {
    path: ':slug_cate/:slug',
    component: TintucDetailComponent
  }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: []
})
export class TintucRoutingModule { }
