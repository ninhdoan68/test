import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TintucNoibatComponent } from './tintuc-noibat/tintuc-noibat.component';

import { TintucDetailComponent } from './tintuc-detail/tintuc-detail.component';
import { TintucRoutingModule } from './tintuc-routing.module';
import {NgxPaginationModule} from 'ngx-pagination';
import { TintucComponent } from './tintuc/tintuc.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '../../shared/shared.module';


@NgModule({
  declarations: [TintucNoibatComponent, TintucDetailComponent, TintucComponent,],
  imports: [
    CommonModule,
    TintucRoutingModule,
    NgxPaginationModule,
    FormsModule,
    NgbModule,
    SharedModule
  ]
})
export class TintucModule { }
