import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CuisineDetailComponent} from './cuisine-detail/cuisine-detail.component';
import {CuisineListComponent} from './cuisine-list/cuisine-list.component';
import {SharedModule} from '../../shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: CuisineListComponent
  },
  {
    path: ':slug',
    component: CuisineDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes), SharedModule],
  exports: [RouterModule]
})
export class CuisineRoutingModule {}
