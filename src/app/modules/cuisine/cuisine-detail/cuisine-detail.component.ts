import {
  Component,
  OnInit
} from '@angular/core';
import {
  CuisineService
} from '../../../shared/services/cuisine.service';
import {
  BaseService
} from '../../../shared/services/base.service';
import {
  FormBuilder
} from '@angular/forms';
import {
  Validators
} from '@angular/forms';
import {
  ActivatedRoute
} from '@angular/router';

@Component({
  selector: 'app-combo-detail',
  templateUrl: './cuisine-detail.component.html',
  styleUrls: ['./cuisine-detail.component.scss']
})
export class CuisineDetailComponent implements OnInit {
  show = false;
  slides = [];
  slideConfig = {
    "slidesToShow": 1,
    "slidesToScroll": 1,
    "asNavFor": '.slide_be',
    "infinity": true,
    "arrows": true,
    autoplay: true,
    autoplaySpeed: 3500
  };
  slideConfig2 = {
    "slidesToShow": 4,
    "slidesToScroll": 1,
    "arrows": false,
    "asNavFor": '.slide_to',
    "infinity": true,
    autoplay: true,
    autoplaySpeed: 3500
  };

  constructor(
    private cs: CuisineService,
    private ActivatedRoute: ActivatedRoute,
    private bs: BaseService,
    private fb: FormBuilder,
  ) {
    this.handleSlug();
  }
  arraySlug = {};
  slug;
  data;
  cuisine;
  num_people = 0;
  event_detail;
  temp_numpeople = '';
  today = new Date();
  date = null;
  full_name = '';
  phone = '';
  email = '';
  notes = '';
  validate_notice = {
    name: '',
    phone: '',
    num: '',
    email: '',
    date: ''
  };
  check_char = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?`~]+/;
  check_num = /[0123456789]+/;

  handleSlug() {
    const slug = this.ActivatedRoute.snapshot.paramMap.get('slug');
    this.cs.getCuisineName(slug).subscribe(event_detail => {
      this.event_detail = event_detail[0];
      this.arraySlug[this.event_detail['slug']] = this.event_detail.name;
    });
  }

  ngOnInit() {
    this.slug = this.ActivatedRoute.snapshot.paramMap.get('slug');
    this.getDetailSlug(this.slug);
  }

  getDetailSlug(slug) {
    this.show = false;
    this.cs.getCuisine(slug).subscribe(data => {
      this.data = data;
      this.slides = [];
      this.data.map(data => {
        if (data.resGalleries) {
          data.resGalleries.map(resGallerie => {
            if (resGallerie.resMedia) {
              resGallerie.resMedia.map(resMed => {
                if (resMed.irAttachments && resMed.irAttachments.length > 0) {
                  let image = this.bs.adminUrl + resMed.irAttachments[0].id;
                  this.slides.push({
                    img: image
                  });
                }
              });
            }
          });
        }

        if (Object.keys(data.irAttachments).length > 0) {
          data['image'] = this.bs.adminUrl + data.irAttachments[0].id;
        } else {
          data['image'] = '';
        }
      });

      this.show = true;
      console.log(this.slides);
      for (let i of this.data) {
        this.slides.push({
          img: i.image
        });
      }
      this.cs.getCuisine3().subscribe(data => {
        this.cuisine = data.filter(value => {
          return value.slug !== this.slug;
        });
        this.cuisine.map(da => {
          da.irAttachments.map(d => {
            da["img"] = this.bs.adminUrl + d.id;
          });
        });
      });
      this.cs.getProductCuisine(this.data[0].id).subscribe( respon => {
        this.data[0]['ser_id'] = respon[0].id;
      });
      console.log('data trả về :', this.data);
    });
  }

  setnumber(bool) {
    this.num_people = (bool ? this.num_people + 1 : (this.num_people > 0 ? this.num_people - 1 : this.num_people));
    this.temp_numpeople = (this.num_people === 0 ? null : this.num_people + ' người');
  }

  request_combo() {
    this.validate_notice = {
      name: '',
      phone: '',
      email: '',
      num: '',
      date: ''
    };
    if (this.full_name.trim() === '') {
      this.validate_notice.name = 'notice_on';
      return;
    }
    if (this.check_num.test(this.full_name.trim()) || this.check_char.test(this.full_name.trim())) {
      this.validate_notice.name = 'notice_on2';
      return;
    }
    if (this.phone.trim() === '') {
      this.validate_notice.phone = 'notice_on';
      return;
    }
    if (isNaN(Number(this.phone)) || this.phone.trim().length !== 10) {
      this.validate_notice.phone = 'notice_on2';
      return;
    }
    if (this.email.trim() !== '' && !this.cs.isEmail(this.email.trim())) {
      this.validate_notice.email = 'notice_on2';
      return;
    }
    if (this.num_people === 0) {
      this.validate_notice.num = 'notice_on';
      return;
    }
    if (this.date === null) {
      this.validate_notice.date = 'notice_on';
      return;
    }
    this.cs.getPartnerByMobile(this.phone).subscribe(
      partner => {
        console.log('infor_partner', partner);
        if (partner.length === 0) {
          const data_request_partner = {
            create_date: this.date,
            name: this.full_name,
            display_name: this.full_name,
            email: this.email,
            mobile: this.phone,
            invoice_warn: "no-message",
            sale_warn: "no-message",
            picking_warn: "no-message"
          };
          this.cs.send_request_partner(data_request_partner).subscribe(response_partner => {
            console.log('response_partner', response_partner);
            this.processPartner(response_partner);
          });
        } else {
          this.processPartner(partner[0]);
        }
      });
  }

  processPartner(partner) {
    this.cs.getSaleOrder().subscribe(a => {
      const b = a[0].id + 1;
      const date = new Date();
      const paramsSale = {
        name: 'SO0' + b,
        department_id: 2,
        date_order: date.toISOString(),
        partner_id: partner.id,
        state: 'draft',
        note: ' Số bàn: ' + this.num_people + '\n Ngày nhận phòng:' + this.date.toLocaleString() + '\n Yêu cầu thêm: ' + this.notes,
        partner_shipping_id: partner.id,
        partner_invoice_id: partner.id,
        pricelist_id: 1,
        team_id: 1,
        warehouse_id: 1,
        picking_policy: 'direct',
        // amount_total:
      };
      this.cs.saveSaleOrder(paramsSale).subscribe(saleResult => {
        console.log('sale order ok ');
        this.cs.getHotelFolio().subscribe(a => {
          const b = a[0].id + 1;
          const paramsHotelFolio = {
            name: 'Folio/00' + b,
            order_id: saleResult.id,
            checkin_date: this.today.toISOString(),
            checkout_date: this.today.toISOString(),
            hotel_policy: "manual",
            note: ' Số lượng bàn: ' + this.num_people + '\n Ngày nhận phòng:' + this.date.toLocaleString() + '\n Yêu cầu thêm: ' + this.notes,
            duration: 1,
            state: 'draft',
            amount_total: this.data[0].price * this.num_people,
            amount_untaxed: this.data[0].price * this.num_people
          };
          this.cs.saveHotelFolio(paramsHotelFolio).subscribe(HotelFolioResult => {
            console.log('Folio ok ');
            this.cs.saveSaleOrderLine(
              {
                customer_lead: 0,
                name: '[' + this.data[0].name + '] - ' + this.date.toLocaleString(),
                order_id: saleResult.id,
                product_id: this.data[0].ser_id,
                product_uom: 29,
                price_unit: this.data[0].price,
                product_uom_qty: this.num_people,
                price_subtotal: this.data[0].price * this.num_people
              }
            ).subscribe(saleLineResult => {
              console.log(saleLineResult);
              const paramServiceLine = {
                service_line_id: saleLineResult.id,
                folio_id: HotelFolioResult.id,
                ser_checkin_date: this.date.toISOString(),
                ser_checkout_date: this.date.toISOString()
              };
              console.log(paramServiceLine);
              this.cs.saveHotelServiceLine(paramServiceLine).subscribe(res => {
                console.log('okok');
              });
            });
          });
        });
      });
    });
  }
}
