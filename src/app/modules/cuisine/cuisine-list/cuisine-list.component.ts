import { Component, OnInit } from '@angular/core';
import { CuisineService } from '../../../shared/services/cuisine.service';
import { BaseService } from '../../../shared/services/base.service';
@Component({
  selector: 'app-combo-list',
  templateUrl: './cuisine-list.component.html',
  styleUrls: ['./cuisine-list.component.scss']
})
export class CuisineListComponent implements OnInit {
  constructor(
    private CuisineService: CuisineService,
    private bs: BaseService,
  ) { }
  slides = [];
  slideConfig = {
    slidesToShow: 4,
    slidesToScroll: 4,
    dots: true,
    infinite: true,
    speed: 500,
    arrows: true,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 376,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  };
  ImgUrl = this.bs.adminUrl;
  cuisine;
  dacsan = [];
  amthuc = [];
  nhahang = [];
  douong = [];
  beverage = [];
  beverage2 = [];
  food;
  restaurant;
  getAllBeverage() {
    this.CuisineService.getBeverage().subscribe(data => {
      data.map(dat => {
        dat.hotelMenucards.map(da => {
          da.irAttachments.map(d => {
            da["ANH"] = `${this.ImgUrl}${d.id}`;
          });
          da["NAME"] = da.productProduct.productTemplate.name;
          da["PRICE"] = da.productProduct.prices;
        });
      });
      this.beverage = data;
      for (let i of this.beverage) {
        for (let k of i.hotelMenucards) {
          this.beverage2.push({ img: k.ANH, price: k.PRICE, name: k.NAME, uom: k.uom_ids });
        }
      }
      console.log('Đồ uống', this.beverage2);

    });
  }
  getAllFood() {
    this.CuisineService.getFood().subscribe(data => {
      data.map(dat => {
        dat.hotelMenucards.map(da => {
          da.irAttachments.map(d => {
            da["ANH"] = `${this.ImgUrl}${d.id}`;
          });
          da["NAME"] = da.productProduct.productTemplate.name;
          da["PRICE"] = da.productProduct.prices;
        });
      });
      this.food = data;
      console.log(this.food);
      for (let i of this.food) {
        for (let k of i.hotelMenucards) {
          this.slides.push({ img: k.ANH, price: k.PRICE, name: k.NAME, uom: k.uom_ids });
        }
      }

      console.log('Đặc Sản', this.slides);


    });
  }
  restaurant2 = [];
  getAllRestaurant() {
    this.CuisineService.getRestaurant().subscribe(data => {
      data.map(dat => {
        dat.hotelMenucards.map(da => {
          da.irAttachments.map(d => {
            da["ANH"] = `${this.ImgUrl}${d.id}`;
          });
          da["NAME"] = da.productProduct.productTemplate.name;
        });
        // this.restaurant2 = dat.hotelMenucards;
      });
      this.restaurant = data;
      for (let i of this.restaurant) {
        for (let k of i.hotelMenucards) {
          this.restaurant2.push(k);
        }
      }
      console.log('test nhà hàng', this.restaurant2);

    });
  }
  getAllCuisine() {
    this.CuisineService.getCuisines().subscribe(data => {
      data.map(da => {
        da.irAttachments.map(d => {
          da["ANH"] = `${this.ImgUrl}${d.id}`;
        });
      });
      this.cuisine = data;
      console.log("Ẩm thực", this.cuisine);

    });
  }
  ngOnInit() {
    this.getAllCuisine();
    this.getAllBeverage();
    this.getAllFood();
    this.getAllRestaurant();
  }

}
