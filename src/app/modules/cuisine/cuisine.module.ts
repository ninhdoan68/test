import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CuisineRoutingModule } from './cuisine-routing.module';
import {CuisineListComponent} from './cuisine-list/cuisine-list.component';
import {CuisineDetailComponent} from './cuisine-detail/cuisine-detail.component';
import {SharedModule} from '../../shared/shared.module';
import { BsDatepickerModule } from 'ngx-bootstrap';
import {FormsModule} from "@angular/forms";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
@NgModule({
  declarations: [CuisineListComponent, CuisineDetailComponent],
  imports: [CommonModule, CuisineRoutingModule, SharedModule, NgbModule, FormsModule, BsDatepickerModule.forRoot(),]
})
export class CuisineModule {}
