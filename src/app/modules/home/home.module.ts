import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeRoutingModule } from './home-routing.module';
import {SharedModule} from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';
import {BsDatepickerModule, BsDropdownModule} from "ngx-bootstrap";
import {HomeComponent} from "./home/home.component";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";

@NgModule({
  declarations: [HomeComponent],
  imports: [
    BsDatepickerModule.forRoot(),
    BsDropdownModule.forRoot(),
    CommonModule,
    SharedModule,
    HomeRoutingModule,
    FormsModule,
    NgbModule
  ]
})
export class HomeModule { }
