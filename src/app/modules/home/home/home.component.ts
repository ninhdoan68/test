import {Component, OnInit} from '@angular/core';
import {HomeService} from '../../../shared/services/home.service';
import {BaseService} from '../../../shared/services/base.service';
import {SearchService} from "../../search/search.service";
import {FormBuilder} from '@angular/forms';
import {ContactService} from '../../../shared/services/contact.service';
import {Validators} from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {

  constructor(private BaseService: BaseService,
              public SearchService: SearchService,
              private HomeService: HomeService,
              private fb: FormBuilder,
              private cs: ContactService,
  ) {
    this.getHomeBanner();
    this.getHomechungtoi();
    this.getBlogcategories();
    this.getHotel();
    this.getCombo();
    this.getAboutUs();

  }

  slideConfig = {
    "slidesToShow": 1,
    "slidesToScroll": 1,
    "dots": true,
    "arrows": true,
    "infinite": true,
    "speed": 1000,
    "autoplay": true,
    "autoplaySpeed": 3000,
  };

  slideConfig2 = {
    "dots": true,
    "speed": 500,
    "slidesToShow": 3,
    "slidesToScroll": 1,
    responsive: [
      {
        breakpoint: 1023,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      },
    ]
  };
  // show = false;
  // isShow = false;
  homebanner;
  array_Hotelroomtype = [];
  today = new Date();
  date = null;
  temp_numpeople = '';
  filter = {
    start_date: null,
    end_date: null,
    num: null
  };
  temp_type_room = 'Chọn loại phòng';
  temp_filter = {
    type_room: '',
    num: 'Số người'
  };
  slider = [];
  image;
  list_type_of_room = [];


  getHomeBanner() {
    this.HomeService.getHomeBanner().subscribe(data => {
      this.homebanner = data;
      console.log(this.homebanner);
      this.homebanner.map(data => {
        if (Object.keys(data.resMedia).length > 0) {

          data.resMedia.map(da => {
            if (Object.keys(da.irAttachments).length > 0) {
              da['image'] = this.BaseService.adminUrl + da.irAttachments[0].id + '/picture';
            } else {
              da['image'] = '';
            }
          });
        } else {
          data['img'] = 'No_image_avalilable.svg';
        }
        this.slider = this.homebanner.filter(dta => {
          return dta.slug === "home-banner-slider";
        });
        this.image = this.homebanner.filter(dta => {
          return dta.slug === "home-gallery";
        });
      });
      //
      // console.log(this.image);
    });
  }

  bannerchungtoi;
  chungtoi;

  getHomechungtoi() {
    this.HomeService.getHomeLoGo().subscribe(data => {
      this.bannerchungtoi = data;
      this.bannerchungtoi.map(da => {
        if (Object.keys(da.irAttachments).length > 0) {
          da['image'] = this.BaseService.adminUrl + da.irAttachments[0].id + '/picture';
        } else {
          da['image'] = '';
        }
      });
      this.chungtoi = this.bannerchungtoi.filter(logo => {
        return logo.slug === "home-vi-sao-chon-thien-son-suoi-nga";
      });
// console.log(this.chungtoi);

    });
  }

  blog;
  tintuc;

  getBlogcategories() {
    this.HomeService.getBlogcategories().subscribe(data => {
      this.blog = data;
      // console.log(this.blog);
      this.blog.map(data => {
        if (Object.keys(data.resBlogs).length > 0) {
          data.resBlogs.map(da => {
            if (Object.keys(da.irAttachments).length > 0) {
              da['image'] = this.BaseService.adminUrl + da.irAttachments[0].id + '/picture';
            } else {
              da['image'] = '';
            }
          });
        } else {
          data['img'] = 'No_image_avalilable.svg';
        }
        this.tintuc = this.blog.filter(da => {
          return da.slug === "tin-tuc-noi-bat";
        });
      });
      // console.log('tin tuc', this.tintuc);
    });
  }

  hotel;
  hotel_large = [];
  hotelnoibat;

  getHotel() {
    this.HomeService.getHotel().subscribe(data => {
      this.hotel = data;
      // console.log(this.hotel);

      this.hotel.map(da => {
        if (Object.keys(da.irAttachments).length > 0) {
          da['image'] = this.BaseService.adminUrl + da.irAttachments[0].id + '/picture';
        } else {
          da['image'] = '';
        }
        this.hotelnoibat = this.hotel.filter(noibat => {
          return noibat.featured === "featured";
        });
        this.hotelnoibat.map( hotel => {
          if (/nha-san/.test(hotel.slug)) {
            hotel.router = `/dat-phong/nha-san/${hotel.slug}`;
          } else if (/phong-hop/.test(hotel.slug)) {
            hotel.router = `/dat-phong/phong-hop/${hotel.slug}`;
          } else {
            hotel.router = `/dat-phong/khach-san/${hotel.slug}`;
          }
        });
        this.price = da.price;
        if (da.resPromotions.length > 0) {
          let count;

          let book_date_from = new Date(da.resPromotions[0].book_date_from).getTime();
          let book_date_to = new Date(da.resPromotions[0].book_date_to).getTime();
          let min_day_book = da.resPromotions[0].min_day_book;

          let now = new Date().getTime();
          {
            da.resPromotions.map(d => {
              this.discount_value = d.discount_value;
              this.discount_type = d.discount_type;

            });

            da["isPromotions"] = true;
            if (this.discount_type === "%") {
              count = Math.round(this.price - (this.price * this.discount_value) / 100);
            } else {
              count = this.price - this.discount_value < 0 ? 0 : this.price - this.discount_value;
            }
            // kiem tra dieu kien
            da["price_promotion"] = count;

            if (da.resPromotions[0].promotion_type === "last") {
              da["isPromotionLast"] = true;
            } else {
              da["isPromotionLast"] = false;
            }
          }
        } else {
          da["isPromotionLast"] = false;
          da["isPromotion"] = false;
          da["price_promotion"] = null;
        }
        if (da.type_of_room === "large_room") {
          this.hotel_large.push(da);
        }

      });

    });
  }

  combo;
  discount_value;
  discount_type;
  price;

  getCombo() {
    this.HomeService.getCombo().subscribe(data => {
      this.combo = data;
      // console.log(this.combo);
      this.combo.map(da => {
        if (Object.keys(da.irAttachments).length > 0) {
          da['image'] = this.BaseService.adminUrl + da.irAttachments[0].id + '/picture';
        } else {
          da['image'] = '';
        }
        this.price = da.price;
        // console.log(this.price);
        if (da.resPromotions.length > 0) {
          let count;

          let book_date_from = new Date(da.resPromotions[0].book_date_from).getTime();
          let book_date_to = new Date(da.resPromotions[0].book_date_to).getTime();
          let min_day_book = da.resPromotions[0].min_day_book;

          let now = new Date().getTime();
          {
            da.resPromotions.map(d => {
              this.discount_value = d.discount_value;
              this.discount_type = d.discount_type;

            });
            // console.log(this.discount_value);

            da["isPromotions"] = true;
            if (this.discount_type === "%") {
              count = Math.round(this.price - (this.price * this.discount_value) / 100);
            } else {
              count = this.price - this.discount_value < 0 ? 0 : this.price - this.discount_value;
            }
            // kiem tra dieu kien
            da["price_promotion"] = count;

            if (da.resPromotions[0].promotion_type === "last") {
              da["isPromotionLast"] = true;
            } else {
              da["isPromotionLast"] = false;
            }
          }
        } else {
          da["isPromotionLast"] = false;
          da["isPromotion"] = false;
          da["price_promotion"] = null;
        }
      });
    });
  }

  dichvu;

  getDichvu() {

  }

  aboutUs;
  getAboutUs() {
    this.HomeService.getAboutUs().subscribe(data => {
      this.aboutUs = data.filter(da => {
        return da.name === "VỀ THIÊN SƠN SUỐI NGÀ ";
      });
      console.log('data', this.aboutUs);
    });
  }
  ContactForm;

  postContact() {
    this.cs.postContact(this.ContactForm.value).subscribe(data => {
      this.cs.addCrmLead(data).subscribe();
    });
    alert("Cảm ơn bạn đã liên hệ với chúng tôi!");
    this.ContactForm.reset();
  }

  ngOnInit() {
    this.getDataFilter();
    this.ContactForm = this.fb.group({
      email: ['',
        Validators.email
      ],
      massage: [''],
    });
    this.HomeService.getTypeofRoom().subscribe(types => {
      types.forEach( type => {
        if (/nha-san/.test(type.slug)) {
          type.router = `/dat-phong/nha-san/${type.hotelRoomTypes[0].slug}`;
        } else if (/phong-hop/.test(type.slug)) {
          type.router = `/dat-phong/phong-hop/${type.hotelRoomTypes[0].slug}`;
        } else {
          type.router = `/dat-phong`;
        }
        if (type.irAttachments) {
          if (Object.keys(type).length > 0 && typeof type !== 'undefined' && type.irAttachments.length > 0) {
            type.image = this.BaseService.adminUrl + type.irAttachments[0].id + '/picture';
          } else {
            type.image = '../assets/image/no-image.png';
          }
        }
      });
      this.list_type_of_room = types;
      console.log('this is type', this.list_type_of_room);
    });
  }

  onToggle() {

  }

  // onToggle2(b) {
  //   this.isShow = b;
  // }

  pickStartdate(date) {
    this.filter.end_date = date;
  }

  setnumber(bool) {
    this.filter.num = (bool ? this.filter.num + 1 : (this.filter.num > 0 ? this.filter.num - 1 : this.filter.num));
    this.temp_numpeople = (this.filter.num === 0 ? null : this.filter.num + ' người');
  }

  sendfilter() {
    this.SearchService.filter = this.filter;
    // console.log(this.SearchService.filter);
  }

  getDataFilter() {
    this.HomeService.getHotelRoomType().subscribe(data => {
      this.array_Hotelroomtype = data;
    });
  }
}
