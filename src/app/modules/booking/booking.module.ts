import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BookingRoutingModule } from './booking-routing.module';
import {SharedModule} from "../../shared/shared.module";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {BsDatepickerModule, BsDropdownModule} from "ngx-bootstrap";
import { HotelComponent } from './hotel/hotel.component';
import { PhongGiaDinhComponent } from './hotel/phong-gia-dinh/phong-gia-dinh.component';
import {Error404RoutingModule} from "../error404/error404-routing.module";
import {NhasanComponent} from "./nhasan/nhasan.component";
import {PhonghopComponent} from "./phonghop/phonghop.component";
import {ThanhtoanComponent} from "./thanhtoan/thanhtoan.component";
@NgModule({
  declarations: [HotelComponent, PhongGiaDinhComponent, NhasanComponent, PhonghopComponent, ThanhtoanComponent],
  imports: [CommonModule, BookingRoutingModule, Error404RoutingModule, SharedModule, NgbModule, BsDropdownModule.forRoot(), FormsModule, BsDatepickerModule.forRoot()],
  exports: []
})
export class BookingModule {}
