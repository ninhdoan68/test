import { Component, OnInit } from '@angular/core';
import {BaseService} from "../../../shared/services/base.service";
import {ActivatedRoute, Router} from "@angular/router";
import {DatPhongService} from "../../../shared/services/dat-phong.service";

@Component({
  selector: 'app-thanhtoan',
  templateUrl: './thanhtoan.component.html',
  styleUrls: ['./thanhtoan.component.scss']
})
export class ThanhtoanComponent implements OnInit {

  constructor(
    private datPhongService: DatPhongService,
    private baseService: BaseService,
    public activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    this.handleSlug();
  }
  today = new Date();
  arraySlug = {};
  room_detail;
  data_user = this.datPhongService.data;
  payBank = false;
  payOne = false;
  check_char = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
  check_num = /[0123456789]+/;
  validate_notice = {
    name: '',
    phone: '',
    email: '',
    address: ''
  };
  full_name = '';
  email = '';
  phone = '';
  address = '';
  list_bank = [];
  selected_bank;
  notes = '';

  ngOnInit() {
    this.handleParam();
  }
  handleSlug() {
    const slug = this.activatedRoute.snapshot.paramMap.get('slug');
    this.datPhongService.getRoomName(slug).subscribe(room_detail => {
      this.room_detail = room_detail[0];
      this.arraySlug[this.room_detail.slug] = this.room_detail.name;
    });
  }

  handleParam() {
    const slug = this.activatedRoute.snapshot.paramMap.get('slug');
    this.datPhongService.getDetailBooking(slug).subscribe( room_detail => {
      this.room_detail = room_detail[0];
      this.room_detail.promotion_price = 0;
      if (this.room_detail.resPromotions.length) {
        if (this.room_detail.resPromotions[0].discount_type === '%') {
          this.room_detail.promotion_price = this.room_detail.price - (this.room_detail.price * this.room_detail.resPromotions[0].discount_value / 100);
        } else {
          this.room_detail.promotion_price = this.room_detail.price - this.room_detail.resPromotions[0].discount_value;
        }
      }
      this.room_detail.total_price = ( this.room_detail.promotion_price !== 0 ? this.room_detail.promotion_price : this.room_detail.price ) * this.data_user.number;
      room_detail.map(item => {
        item.hotelRoomAmenities.map( amenity => {
          this.datPhongService.getProductHotelAmenity(amenity.product_id).subscribe( prod_amenity => {
            amenity.name = prod_amenity[0].title_name;
          });
          if (Object.keys(amenity) && typeof amenity !== 'undefined' && amenity.irAttachments.length > 0) {
            amenity.image = this.baseService.adminUrl + amenity.irAttachments[0].id + '/picture';
          } else {
            amenity.image = '';
          }
        });
      });
      console.log(this.room_detail);

    });
    this.getBank();
  }

  getBank() {
    this.datPhongService.getBank().subscribe( banks => {
      banks.map( bank => {
        if (Object.keys(bank) && typeof bank !== 'undefined' && bank.resBank.irAttachments.length > 0) {
          bank.image = this.baseService.adminUrl + bank.resBank.irAttachments[0].id + '/picture';
        } else {
          bank.image = '../assets/image/no-image.png';
        }
      });
      this.list_bank = banks;
      console.log('bank : ', this.list_bank);
    });
  }

  toggleBank(b) {
    if (b) {
      this.payBank = !this.payBank;
      this.payOne = false;
    } else {
      this.payOne = !this.payOne;
      this.payBank = false;
    }
  }

  set_bank(b) {
    this.selected_bank = b;
  }

  request_booking() {
    this.validate_notice = {
      name: '',
      phone: '',
      email: '',
      address: ''
    };
    if (this.full_name.trim() === '') {
      this.validate_notice.name = 'notice_on';
      return;
    }
    if (this.check_num.test(this.full_name.trim()) || this.check_char.test(this.full_name.trim())) {
      this.validate_notice.name = 'notice_on2';
      return;
    }
    if (this.phone.trim() === '') {
      this.validate_notice.phone = 'notice_on';
      return;
    }
    if (isNaN(Number(this.phone)) || this.phone.trim().length !== 10) {
      this.validate_notice.phone = 'notice_on2';
      return;
    }
    if (this.email.trim() !== '' && !this.datPhongService.isEmail(this.email.trim())) {
      this.validate_notice.email = 'notice_on2';
      return;
    }
    if (this.check_char.test(this.address.trim())) {
      this.validate_notice.address = 'notice_on2';
      return;
    }
    this.notes = '  Địa chỉ : ' + this.address + '\n  Ngày nhận phòng : ' + this.data_user.start_date.toLocaleString() +
      '\n  Ngày trả phòng : ' + this.data_user.end_date.toLocaleString() + '\n Số lượng : ' + this.data_user.number + '\n';
    this.datPhongService.getPartnerByMobile(this.phone).subscribe(
      partner => {
        console.log('infor_partner', partner);
        if (partner.length === 0) {
          const data_request_partner = {
            create_date: this.today,
            name: this.full_name,
            display_name: this.full_name,
            email: this.email,
            mobile: this.phone,
            invoice_warn: "no-message",
            sale_warn: "no-message",
            picking_warn: "no-message"
          };
          this.datPhongService.send_request_partner(data_request_partner).subscribe(response_partner => {
            console.log('response_partner', response_partner);
            this.baseService.updatePartner(response_partner).subscribe(rateupdate => {
              console.log('rateupdate', rateupdate);
            });
            this.processPartner(response_partner);
          });
        } else {
          this.processPartner(partner[0]);
        }
      });
    if (this.payBank) {
      this.notes = this.notes + ' Hình thức thanh toán : \n -   Chuyển khoản ngân hàng : \n     • ' +
      this.selected_bank.resBank.name + '\n     • Số TK : ' + this.selected_bank.acc_number + '\n     • Chủ TK : ' +
        this.selected_bank.resPartner.display_name ;
    }
    if (this.payOne) {
      this.notes = this.notes + ' Hình thức thanh toán : PayOne';
    }
    console.log('note: ', this.notes);
  }

  processPartner(partner) {
    this.datPhongService.getHotelReservations().subscribe(a => {
      const b = a[0].id + 1;
      const date = new Date();
      const paramsSale = {
        reservation_no: 'R/000' + b,
        date_order: date.toISOString(),
        partner_id: partner.id,
        state: 'draft',
        description: 'Số người: ' + this.data_user.number + '\n Email: ' + this.email +
          '\n Yêu cầu thêm : ' + this.notes,
        adults: this.data_user.number,
        checkin: this.data_user.start_date.toISOString(),
        checkout: this.data_user.end_date.toISOString(),
        children: 0,
        partner_shipping_id: partner.id,
        partner_invoice_id: partner.id,
        pricelist_id: 1,
        warehouse_id: this.room_detail.stockWarehouse.id,
      };
      this.datPhongService.saveHotelReservations(paramsSale).subscribe(saleResult => {
        console.log('hotel reservtion ok ');
        this.router.navigate(['/thank-you']);
      });
    });
  }
}
