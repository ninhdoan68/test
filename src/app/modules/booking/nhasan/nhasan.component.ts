import {Component, OnInit} from '@angular/core';
import {DatPhongService} from "../../../shared/services/dat-phong.service";
import {BaseService} from "../../../shared/services/base.service";
import {ActivatedRoute, Router, RouterLink} from "@angular/router";
import {fakeAsync} from "@angular/core/testing";

declare var $: any;

@Component({
  selector: 'app-nhasan',
  templateUrl: './nhasan.component.html',
  styleUrls: ['./nhasan.component.scss']
})
export class NhasanComponent implements OnInit {
  constructor(
    private datphongService: DatPhongService,
    private baseService: BaseService,
    public activatedRoute: ActivatedRoute,
    private router: Router
  ) {


    this.handleSlug();
  }

  arraySlug = {};
  slideConfigBlog1 = {
    'slidesToShow': 1,
    'slidesToScroll': 1,
    'arrows': true,
    'asNavFor': '.carousel2',
    'autoplay': true,
    'autoplaySpeed': 8000
  };
  slideConfigBlog2 = {
    'slidesToShow': 4,
    'slidesToScroll': 1,
    'asNavFor': '.carousel1',
    'dots': false,
    'autoplay': true,
    'focusOnSelect': true,
    'autoplaySpeed': 8000
  };
  nhasan_Images = [];
  nhasan_detail;
  num_people = 0;
  temp_numpeople = '';
  today = new Date();
  date = null;
  full_name = '';
  phone = '';
  email = '';
  notes = '';
  validate_notice = {
    name: '',
    phone: '',
    num: '',
    email: '',
    date: '',
    date1: ''
  };
  onday = false;
  onnight = false;
  ontime = '';
  select_warehouse = {
    ha : false,
    trung : false,
    ngoa : false,
    no : 0
  };
  check_char = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?`~]+/;
  check_num = /[0123456789]+/;

  async ngOnInit() {
    this.handleParam();
  }

  handleSlug() {
    const slug = this.activatedRoute.snapshot.paramMap.get('slug');
    this.datphongService.getRoomName(slug).subscribe(nhasan_detail => {
      this.nhasan_detail = nhasan_detail[0];
      if (this.nhasan_detail) {
        this.arraySlug[this.nhasan_detail.slug] = this.nhasan_detail.name;
      }
    });
  }

  handleParam() {
    const slug = this.activatedRoute.snapshot.paramMap.get('slug');
    this.datphongService.getDetailBooking(slug).subscribe(nhasan_detail => {
      this.nhasan_detail = nhasan_detail[0];
      if (this.nhasan_detail) {
        console.log(this.nhasan_detail);
        if (this.nhasan_detail.resGalleries.length) {
          this.nhasan_detail.resGalleries.map(gall => {
            if (gall.resMedia.length) {
              gall.resMedia.map(media => {
                if (media.irAttachments.length) {
                  media.irAttachments.map(img => {
                    const imageItem = {
                      image: this.baseService.adminUrl + img.id,
                    };
                    this.nhasan_Images.push(imageItem);
                  });
                }
              });
            }
          });
        }
      }
    });
  }

  request_booking() {
    console.log('aaaaaa');
    this.validate_notice = {
      name: '',
      phone: '',
      num: '',
      email: '',
      date: '',
      date1: ''
    };
    if (this.full_name.trim() === '') {
      this.validate_notice.name = 'notice_on';
      return;
    }
    if (this.check_num.test(this.full_name.trim()) || this.check_char.test(this.full_name.trim())) {
      this.validate_notice.name = 'notice_on2';
      return;
    }
    if (this.phone.trim() === '') {
      this.validate_notice.phone = 'notice_on';
      return;
    }
    if (isNaN(Number(this.phone)) || this.phone.trim().length !== 10) {
      this.validate_notice.phone = 'notice_on2';
      return;
    }
    if (this.email.trim() !== '' && !this.datphongService.isEmail(this.email.trim())) {
      this.validate_notice.email = 'notice_on2';
      return;
    }
    if (this.num_people === 0) {
      this.validate_notice.num = 'notice_on';
      return;
    }
    if (this.date === '') {
      this.validate_notice.date = 'notice_on';
      return;
    }

    this.datphongService.getPartnerByMobile(this.phone).subscribe(
      partner => {
        console.log('infor_partner', partner);
        if (partner.length === 0) {
          const data_request_partner = {
            create_date: this.today,
            name: this.full_name,
            display_name: this.full_name,
            email: this.email,
            mobile: this.phone,
            invoice_warn: "no-message",
            sale_warn: "no-message",
            picking_warn: "no-message"
          };
          this.datphongService.send_request_partner(data_request_partner).subscribe(response_partner => {
            console.log('response_partner', response_partner);
            this.processPartner(response_partner);
          });
        } else {
          this.processPartner(partner[0]);
        }
      });
  }

  setnumber(bool) {
    this.num_people = (bool ? this.num_people + 1 : (this.num_people > 0 ? this.num_people - 1 : this.num_people));
    this.temp_numpeople = (this.num_people === 0 ? null : this.num_people + ' người');
  }

  set_time(b) {
    if (b) {
      this.onday = !this.onday;
      this.onnight = false;
    } else {
      this.onnight = !this.onnight;
      this.onday = false;
    }
    this.ontime = this.onday ? 'Trong ngày' : ( this.onnight ? 'Qua đêm' : '' );
  }

  set_warehouse(string) {
    this.select_warehouse.ha = false;
    this.select_warehouse.trung = false;
    this.select_warehouse.ngoa = false;
    this.select_warehouse[`${string}`] = !this.select_warehouse[`${string}`];
    switch (string) {
      case 'ngoa':
        this.select_warehouse.no = 1;
        break;
      case 'trung':
        this.select_warehouse.no = 3;
        break;
      case 'ha':
        this.select_warehouse.no = 2;
        break;
    }
  }

  processPartner(partner) {
    this.datphongService.getHotelReservations().subscribe(a => {
      const b = a[0].id + 1;
      const date = new Date();
      const paramsSale = {
        reservation_no: 'R/000' + b,
        date_order: date.toISOString(),
        partner_id: partner.id,
        state: 'draft',
        description: 'Số người: ' + this.num_people + '\n Loại phòng: ' + this.nhasan_detail.name + '\n Thời gian: ' + this.ontime +
          '\n Yêu cầu thêm : ' + this.notes,
        adults: this.num_people,
        checkin: this.date.toISOString(),
        checkout: this.date.toISOString(),
        children: 0,
        partner_shipping_id: partner.id,
        partner_invoice_id: partner.id,
        pricelist_id: 1,
        warehouse_id: this.select_warehouse.no,
      };
      this.datphongService.saveHotelReservations(paramsSale).subscribe(saleResult => {
        console.log('hotel reservtion ok ');
        this.router.navigate(['/thank-you']);
      });
    });
  }
}
