import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HotelComponent} from './hotel/hotel.component';
import {PhongGiaDinhComponent} from './hotel/phong-gia-dinh/phong-gia-dinh.component';
import {ThanhtoanComponent} from "./thanhtoan/thanhtoan.component";
import {NhasanComponent} from "./nhasan/nhasan.component";
import {PhonghopComponent} from "./phonghop/phonghop.component";
import {Error404Component} from "../error404/error404/error404.component";

const routes: Routes = [
  {path: '', component: HotelComponent},
  {
    path: "khach-san",
    children: [
      {
        path: "",
        component: HotelComponent,
      },
      {
        path: ":slug",
        children: [
          {
            path: "",
            component: PhongGiaDinhComponent,
          },
          {
            path: "checkout",
            component: ThanhtoanComponent,
          }
        ]
      }
    ]
  },
  {
    path: "nha-san",
    children: [
      {
        path: "",
        component: Error404Component,
      },
      {
        path: ":slug",
        component: NhasanComponent,
      }
    ]
  },
  {
    path: "phong-hop",
    children: [
      {
        path: "",
        component: Error404Component,
      },
      {
        path: ":slug",
        component: PhonghopComponent,
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BookingRoutingModule {
}
