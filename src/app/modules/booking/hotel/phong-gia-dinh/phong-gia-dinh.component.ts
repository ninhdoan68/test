import {Component, OnInit} from '@angular/core';
import {DatPhongService} from "../../../../shared/services/dat-phong.service";
import {BaseService} from "../../../../shared/services/base.service";
import {ActivatedRoute, Router, RouterLink} from "@angular/router";

declare var $: any;

@Component({
  selector: 'app-phong-gia-dinh',
  templateUrl: './phong-gia-dinh.component.html',
  styleUrls: ['./phong-gia-dinh.component.scss']
})
export class PhongGiaDinhComponent implements OnInit {
  constructor(
    private phonggiadinhService: DatPhongService,
    private baseService: BaseService,
    public activatedRoute: ActivatedRoute,
    private router: Router
  ) {


    this.handleSlug();
  }

  arraySlug = {};
  slideConfigBlog1 = {
    'slidesToShow': 1,
    'slidesToScroll': 1,
    'arrows': true,
    'asNavFor': '.carousel2',
    'autoplay': true,
    'autoplaySpeed': 8000
  };
  slideConfigBlog2 = {
    'slidesToShow': 4,
    'slidesToScroll': 1,
    'asNavFor': '.carousel1',
    'dots': false,
    'autoplay': true,
    'focusOnSelect': true,
    'autoplaySpeed': 8000
  };
  phonggiadinh_Images = [];
  phonggiadinh_detail;
  num_people = 0;
  temp_numpeople = '';
  temp_numpeople2 = '';
  today = new Date();
  date = null;
  full_name = '';
  phone = '';
  email = '';
  notes = '';
  num_people2 = 0;
  datenhanphong = null;
  datenhanphong1 = '';
  datetraphong = null;
  datetraphong1 = '';
  validate_notice = {
    name: '',
    phone: '',
    num: '',
    email: '',
    date: '',
    date1: ''
  };
  validate_notice2 = {
    num2: '',
    date2: '',
    date3: ''
  };
  check_char = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?`~]+/;
  check_num = /[0123456789]+/;

  async ngOnInit() {
    this.handleParam();
  }

  handleSlug() {
    this.activatedRoute.params.subscribe(phonggiadinh => {
      const slug = this.activatedRoute.snapshot.paramMap.get('slug');
      this.phonggiadinhService.getRoomName(slug).subscribe(phonggiadinh_detail => {
        this.phonggiadinh_detail = phonggiadinh_detail[0];
        if (this.phonggiadinh_detail) {
          this.arraySlug[this.phonggiadinh_detail.slug] = this.phonggiadinh_detail.name;
        }
      });
    });
  }

  handleParam() {
    this.activatedRoute.params.subscribe(phonggiadinh => {
      const slug = this.activatedRoute.snapshot.paramMap.get('slug');
      this.phonggiadinhService.getDetailBooking(slug).subscribe(phonggiadinh_detail => {
        this.phonggiadinh_detail = phonggiadinh_detail[0];
        if (this.phonggiadinh_detail) {
          console.log(this.phonggiadinh_detail);
          if (this.phonggiadinh_detail.resGalleries.length) {
            this.phonggiadinh_detail.resGalleries.map(gall => {
              if (gall.resMedia.length) {
                gall.resMedia.map(media => {
                  if (media.irAttachments.length) {
                    media.irAttachments.map(img => {
                      const imageItem = {
                        image: this.baseService.adminUrl + img.id,
                      };
                      this.phonggiadinh_Images.push(imageItem);
                    });
                  }
                });
              }
            });
          }
        }

      });
    });
  }

  request_booking() {
    this.validate_notice = {
      name: '',
      phone: '',
      num: '',
      email: '',
      date: '',
      date1: ''
    };
    if (this.full_name.trim() === '') {
      this.validate_notice.name = 'notice_on';
      return;
    }
    if (this.check_num.test(this.full_name.trim()) || this.check_char.test(this.full_name.trim())) {
      this.validate_notice.name = 'notice_on2';
      return;
    }
    if (this.phone.trim() === '') {
      this.validate_notice.phone = 'notice_on';
      return;
    }
    if (isNaN(Number(this.phone)) || this.phone.trim().length !== 10) {
      this.validate_notice.phone = 'notice_on2';
      return;
    }
    if (this.email.trim() !== '' && !this.phonggiadinhService.isEmail(this.email.trim())) {
      this.validate_notice.email = 'notice_on2';
      return;
    }
    if (this.num_people === 0) {
      this.validate_notice.num = 'notice_on';
      return;
    }
    if (this.datetraphong === '') {
      this.validate_notice.date = 'notice_on';
      return;
    }
    if (this.datenhanphong === '') {
      this.validate_notice.date1 = 'notice_on';
      return;
    }

    this.phonggiadinhService.getPartnerByMobile(this.phone).subscribe(
      partner => {
        console.log('infor_partner', partner);
        if (partner.length === 0) {
          const data_request_partner = {
            create_date: this.today,
            name: this.full_name,
            display_name: this.full_name,
            email: this.email,
            mobile: this.phone,
            invoice_warn: "no-message",
            sale_warn: "no-message",
            picking_warn: "no-message"
          };
          this.phonggiadinhService.send_request_partner(data_request_partner).subscribe(response_partner => {
            console.log('response_partner', response_partner);
            this.processPartner(response_partner);
          });
        } else {
          this.processPartner(partner[0]);
        }
      });
  }

  setnumber(bool) {
    this.num_people = (bool ? this.num_people + 1 : (this.num_people > 0 ? this.num_people - 1 : this.num_people));
    this.temp_numpeople = (this.num_people === 0 ? null : this.num_people + ' người');
  }

  setnumber2(bool) {
    this.num_people2 = (bool ? this.num_people2 + 1 : (this.num_people2 > 0 ? this.num_people2 - 1 : this.num_people2));
    this.temp_numpeople2 = (this.num_people2 === 0 ? null : this.num_people2 + ' người');
  }

  xemgia() {
    this.validate_notice2 = {
      num2: '',
      date2: '',
      date3: ''
    };
    if (this.datetraphong1 === '') {
      this.validate_notice2.date2 = 'notice_on';
      return;
    }
    if (this.datenhanphong1 === '') {
      this.validate_notice2.date3 = 'notice_on';
      return;
    }
    if (this.num_people2 === 0) {
      this.validate_notice2.num2 = 'notice_on';
      return;
    }
    this.phonggiadinhService.data.start_date = this.datenhanphong1;
    this.phonggiadinhService.data.end_date = this.datetraphong1;
    this.phonggiadinhService.data.number = this.num_people2;
    console.log('test data', this.phonggiadinhService.data);
    this.router.navigate(['/dat-phong/khach-san/' + this.phonggiadinh_detail.slug + '/checkout']);
  }

  processPartner(partner) {
    this.phonggiadinhService.getHotelReservations().subscribe(a => {
      const b = a[0].id + 1;
      const date = new Date();
      const paramsSale = {
        reservation_no: 'R/000' + b,
        date_order: date.toISOString(),
        partner_id: partner.id,
        state: 'draft',
        description: 'Số người: ' + this.num_people + '\n Loại phòng: ' + this.phonggiadinh_detail.name +
          '\n Yêu cầu thêm : ' + this.notes,
        adults: this.num_people,
        checkin: this.datenhanphong.toISOString(),
        checkout: this.datetraphong.toISOString(),
        children: 0,
        partner_shipping_id: partner.id,
        partner_invoice_id: partner.id,
        pricelist_id: 1,
        warehouse_id: this.phonggiadinh_detail.stockWarehouse.id,
      };
      this.phonggiadinhService.saveHotelReservations(paramsSale).subscribe(saleResult => {
        console.log('hotel reservtion ok ');
        this.router.navigate(['/thank-you']);
      });
    });
  }
}
