import { Component, OnInit} from '@angular/core';
import { DatPhongService } from '../../../shared/services/dat-phong.service';
import {BaseService} from '../../../shared/services/base.service';

@Component({
  selector: 'app-hotel',
  templateUrl: './hotel.component.html',
  styleUrls: ['./hotel.component.scss']
})
export class HotelComponent implements OnInit {
  constructor(
    private datphongService: DatPhongService,
    private baseService: BaseService
  ) {}
  adminUrl = this.baseService.adminUrl;
  hero_banner;
  list_allBooking;
  list_booking_HS = [];
  list_booking_TS = [];
  list_booking_NS = [];
  ngOnInit() {
    this.handleParam();
  }
  handleParam() {
      this.datphongService.getHeroBannerBooking().subscribe( herro_banner => {
        console.log(herro_banner);
        if (typeof herro_banner !== 'undefined' && herro_banner[0].irAttachments.length > 0) {
          herro_banner.image = this.baseService.adminUrl + herro_banner[0].irAttachments[0].id + '/picture';
        } else {
          herro_banner.image = '../assets/image/no-image.png';
        }
        this.hero_banner = herro_banner;

      });

      this.datphongService.getBooking().subscribe( bookings => {
        bookings.forEach( item_booking => {
          if (Object.keys(item_booking).length > 0 && typeof item_booking !== 'undefined' && item_booking.irAttachments.length > 0) {
            item_booking.image = this.baseService.adminUrl + item_booking.irAttachments[0].id;
          } else {
            item_booking.image = '../assets/image/no-image.png';
          }
        });
        bookings.map(item => {
          item.hotelRoomAmenities.map( amenity => {
            this.datphongService.getProductHotelAmenity(amenity.product_id).subscribe( prod_amenity => {
              amenity.name = prod_amenity[0].title_name;
            });
            if (Object.keys(amenity).length > 0 && typeof amenity !== 'undefined' && amenity.irAttachments.length > 0) {
              amenity.image = this.baseService.adminUrl + amenity.irAttachments[0].id + '/picture';
            } else {
              // amenity.image = '../assets/image/no-image.png';
            }
          });
          item.retail_price = new Intl.NumberFormat('vi-VN', {
            style: 'currency',
            currency: 'VND'
          }).format(item.price);
        });
        this.list_allBooking = bookings;
        bookings.forEach( booking => {
          if (booking.stockWarehouse) {
            if (booking.stockWarehouse.name === 'Hạ Sơn') {
              this.list_booking_HS.push(booking);
            } else if (booking.stockWarehouse.name === 'Trung Sơn') {
              this.list_booking_TS.push(booking);
            } else {
              this.list_booking_NS.push(booking);
            }
          }
        });

        // console.log(this.list_allBooking);
      });

  }
}
