import { Component, OnInit} from '@angular/core';
import {ComboService} from '../../../shared/services/combo.service';
import {BaseService} from '../../../shared/services/base.service';
import {ThankyouService} from "../../../shared/services/thankyou.service";

@Component({
  selector: 'app-thankyou',
  templateUrl: './thankyou.component.html',
  styleUrls: ['./thankyou.component.scss']
})
export class ThankyouComponent implements OnInit {
  constructor(
    private ThankService: ThankyouService,
    private baseService: BaseService
  ) {}
  hero_banner;
  ngOnInit() {
    this.handleParam();
  }
  handleParam() {
      this.ThankService.getHeroBannerCombo().subscribe( herro_banner => {
        if (Object.keys(herro_banner[0]).length > 0 && typeof herro_banner !== 'undefined' && herro_banner[0].irAttachments.length > 0) {
          herro_banner.image = this.baseService.adminUrl + herro_banner[0].irAttachments[0].id + '/picture';
        } else {
          herro_banner.image = '../assets/image/no-image.png';
        }
        this.hero_banner = herro_banner;

      });
  }
}
