import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedModule} from "../../shared/shared.module";
import {ThankyouComponent} from "./thankyou/thankyou.component";
import { ThankyouRoutingModule } from './thankyou-routing.module';

@NgModule({
  declarations: [ThankyouComponent],
  imports: [CommonModule, SharedModule, ThankyouRoutingModule],
  exports: []
})
export class ThankyouModule {}
