import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SharedModule} from "../../shared/shared.module";
import {ThankyouComponent} from "./thankyou/thankyou.component";

const routes: Routes = [
  {
    path: '',
    component: ThankyouComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), SharedModule],
  exports: [RouterModule],
  declarations: [],
})
export class ThankyouRoutingModule {}
