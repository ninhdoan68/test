import {Component, OnInit} from "@angular/core";
import {BaseService} from "../../../shared/services/base.service";
import {SearchService} from "../search.service";

declare var $: any;
@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.scss']
})
export class SearchResultComponent implements OnInit {
  filter = {
    start_date : null,
    end_date : null,
    num : null
  };
  list_show = [];
  hotel = [];
  nha_san = [];
  phong_hop = [];
  constructor(
    public searchService: SearchService,
    private baseService: BaseService
  ) {
  }
  ngOnInit() {
    this.searchService.getBooking().subscribe( data => {
      this.filter = this.searchService.filter;
      data.forEach( item => {
        if (Object.keys(item).length > 0 && typeof item !== 'undefined' && item.irAttachments.length > 0) {
          item.image = this.baseService.adminUrl + item.irAttachments[0].id + '/picture';
        } else {
          item.image = '../assets/image/no-image.png';
        }
      });
      // console.log('sda', data);
      data.map(item => {
        item.hotelRoomAmenities.map( amenity => {
          this.searchService.getProductHotelAmenity(amenity.product_id).subscribe( prod_amenity => {
            amenity.name = prod_amenity[0].title_name;
          });
          if (Object.keys(amenity && typeof amenity !== 'undefined' && amenity.irAttachments.length > 0)) {
            amenity.image = this.baseService.adminUrl + amenity.irAttachments[0].id + '/picture';
          } else {
            amenity.image = '../assets/image/no-image.png';
          }
        });
      });
      this.nha_san = data.filter( room => {
        return /nha-san/.test(room.slug);
      });
      this.phong_hop = data.filter( room => {
        return /phong-hop/.test(room.slug);
      });
      this.hotel = data.filter( room => {
        return (!(/phong-hop/.test(room.slug)) && !(/nha-san/.test(room.slug)));
      });
      this.hotel.forEach( item => {
        if (
          ( this.filter.start_date === null || item.check_in <= this.filter.start_date.toISOString() ) &&
          ( this.filter.end_date === null || item.check_out >= this.filter.end_date.toISOString() ) &&
          ( this.filter.num === null || item.capacity <= this.filter.num || item.capacity === null)
        ) {
          this.list_show.push(item);
        }
      });
      // console.log('data filter', this.list_show);
      // console.log('filter', this.filter);
    });
  }
}
