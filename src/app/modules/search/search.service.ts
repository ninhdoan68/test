import { Injectable } from "@angular/core";
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {BaseService} from "../../shared/services/base.service";
@Injectable({
  providedIn: "root"
})
export class SearchService {

  filter = {
    start_date : null,
    end_date : null,
    num : null
  };

  token = this.baseService.tokenApi;
  apiUrl = this.baseService.apiUrl;

  constructor(
    private httpClient: HttpClient, public baseService: BaseService,
  ) {}

  getData(table, params) {
    const urlApi = this.apiUrl;
    return this.httpClient.get<any>(encodeURI(`${urlApi}${table}?access_token=${this.token}&filter=${JSON.stringify(params)}`));
  }

  getBooking(): Observable<any> {
    const param = {
      include: [
        {
          relation: 'irAttachments',
          scope: {
            where: {
              res_model: { eq: 'hotel.room.type' }
            },
            order: 'id DESC'
          }
        },
        {
          relation: 'stockWarehouse',
        },
        {
          relation: 'hotelRoomAmenities',
          scope: {
            include: {
              relation: 'irAttachments',
              scope: {
                where: {
                  res_model: { eq: 'hotel.room.amenities' }
                },
                order: 'id DESC'
              }
            }
          }
        },
      ],
    };
    return this.getData('hotel_room_types', param);
  }

  getProductHotelAmenity(id): Observable<any> {
    const param = {
      where: {
        and: [
          {id: { eq: id }}
          // {res_model: { eq: 'res.event.line' }}
        ]
      },
    };
    return this.getData('product_products', param);
  }

}
