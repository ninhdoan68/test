import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { LibraryRoutingModule } from './library-routing.module';
import {NgxGalleryModule} from "ngx-gallery";
import {LibraryComponent} from "./library/library.component";
import 'hammerjs';


@NgModule({
  declarations: [LibraryComponent],
  imports: [
    CommonModule,
    LibraryRoutingModule,
    NgxGalleryModule,
    SharedModule
  ],
})
export class LibraryModule { }
