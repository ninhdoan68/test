import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LibraryComponent } from './library/library.component';
import { SharedModule } from '../../shared/shared.module';
const routes: Routes = [
  {
    path: "",
    component: LibraryComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes), SharedModule],
  exports: [RouterModule],
  declarations: []
})
export class LibraryRoutingModule { }
