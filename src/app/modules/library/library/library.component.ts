import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {LibraryService} from '../../../shared/services/library.service';
import {BaseService} from '../../../shared/services/base.service';
// import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {NgxGalleryAnimation} from "ngx-gallery";
import {DomSanitizer, SafeResourceUrl} from "@angular/platform-browser";

@Component({
  selector: 'app-library',
  templateUrl: './library.component.html',
  styleUrls: ['./library.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LibraryComponent implements OnInit {

  constructor(
    private LibraryService: LibraryService,
    private BS: BaseService,
    private DS: DomSanitizer,
  ) {
  }

  list_gallery_image = [];
  max_image = 6;
  max_video = 6;
  videos;
  galleryImages = [];
  galleryOptions = [
    {
      preview: false,
      width: '100%',
      height: '100%',
      imageAnimation: NgxGalleryAnimation.Slide,
      thumbnailsColumns: 5.5,
      imageArrows: true,
      imageSwipe: true,
      thumbnailsArrows: false,
      thumbnailsSwipe: true,
      imageSize: 'cover',
      imageAutoPlay: true,
      imageAutoPlayInterval: 4000,
      imageAutoPlayPauseOnHover: true,
      imageInfinityMove: true,
      fullscreenIcon: 'fa fa-arrows-alt',
      previewAutoPlayInterval: 4000,
      imageArrowsAutoHide: false
    },
  ];
  safeURL: SafeResourceUrl;

  xemthemVideo() {

  }

  xemthem(b) {
    if (b) {
      this.max_image += 3;
    } else {
      this.max_video += 3;
    }
  }

  getAllImages() {
    this.LibraryService.getImage().subscribe(data => {
      data[0].resGalleries.map(gall => {
        if (gall.resMedia.length) {
          gall.resMedia.map(media => {
            if (media.irAttachments.length) {
              media.irAttachments.map(img => {
                media.image = this.BS.adminUrl + img.id;
              });
            }
          });
          let index = 0;
          for (let media of gall.resMedia) {
            if (media.is_main === '1') {
              media.main = true;
              break;
            } else {
              index = index + 1;
            }
          }
          if (index === gall.resMedia.length) {
            gall.resMedia[0].main = true;
          }
        }
      });
      this.list_gallery_image = data[0].resGalleries;
    });
  }

  getAllVideos() {
    this.LibraryService.getVideo().subscribe(data => {
      this.videos = data[0];
      console.log(this.videos);
      // console.log(this.videos);
      this.videos.resGalleries.map(da => {
        da['image'] = this.getImageYoutube(da.resMedia[0].video);
        da['videoIframe'] = this.genUrlIframeYoutube(da.resMedia[0].video);
      });
    });
  }

  setGallery(gallery) {
    this.galleryImages = [];
    gallery.map(media => {
      if (media.irAttachments.length) {
        media.irAttachments.map(img => {
          const imageItem = {
            small: this.BS.adminUrl + img.id,
            medium: this.BS.adminUrl + img.id,
            big: this.BS.adminUrl + img.id,
          };
          this.galleryImages.push(imageItem);
        });
      }
    });
  }

  setGalleryVideo(video) {
    this.safeURL = this.DS.bypassSecurityTrustResourceUrl(video);

  }

  getImageYoutube(video) {
    return 'https://i.ytimg.com/vi/' + this.getKeyVideoYoutube(video) + '/hqdefault.jpg';
  }

  genUrlIframeYoutube(string) {
    return "https://www.youtube.com/embed/" + this.getKeyVideoYoutube(string);
    // console.log('hhhhh', string);
  }

  getKeyVideoYoutube(string) {
    // console.log(string);
    if (string.replace('embed', '') !== string) {
      let array = string.split('/');
      return array[array.length - 1];
    } else {
      let array = string.split('=');
      let array2 = array[1].split('&');
      return array2[0];
    }

  }

  ngOnInit() {
    this.getAllImages();
    this.getAllVideos();
  }

}
