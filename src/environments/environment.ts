// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: 'https://dev.io/api', // Dev
  server: {
    apiUrl: 'https://10.0.10.32:3004/api/',
    adminUrl: 'https://10.0.10.32:8050/web/image/',
    baseUrl: 'https://dulichdailuc.com',
    tokenApi: '8ktHqwZ0E3Sy7Y1LpWQRyJXoVsn527b0PNOAAkZknp0tATKsqJ4fztMviSseUIPA',
  },
  client: {
    apiUrl: 'https://apihotelmanagerment.azsolutions.vn/api/',
    adminUrl: 'https://adminhotelmanagement.azsolutions.vn/web/image/',
    baseUrl: 'http://thienson.azsolutions.vn',
    tokenApi: '8ktHqwZ0E3Sy7Y1LpWQRyJXoVsn527b0PNOAAkZknp0tATKsqJ4fztMviSseUIPA',
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
